#!/bin/sh
#tableau4.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
# 
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: andre.connes@toulouse.iufm.fr
#  Date    : 24/04/2002 Modification : 06/03/2005
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   Andre Connes
#  @project    Le terrier
#  @copyright  Eric Seigne 24/04/2002
# 
#  *************************************************************************

set didacticiel [lindex $argv 0]
set difficulte [lindex $argv 1]

source tableaux.conf
source msg.tcl

if { $glob(platform) == "windows" } {
  set f [open [file join $glob(home_reglages) trace_user] "r"]
  set glob(trace_user) [gets $f]
  close $f
}

# nbquestions : nbre de questions � poser
# iquestion : num�ro de la question courante
# nbessai : 1 ou 2 essais pour r�pondre � une question
# nbjustes : nbre de r�ponses justes

#set nbessai 0
set iquestion 0
set nbquestions 8
set nbjustes 0

########################### On cr�e la fen�tre principale###########################

# placement de la fen�tre en haut et � gauche de l'�cran

wm resizable . 0 0
wm geometry . [expr int([winfo vrootwidth .]*0.99)]x[expr int([winfo vrootheight .]*0.9)]+0+0
. configure -bg $glob(bgcolor)

frame .frame -width $glob(width) -height $glob(height) -bg $glob(bgcolor)
pack .frame -side top -fill both -expand yes


########################On cr�e un canvas###########################################
# charg� d'accueillir les sorties graphiques,
# qui peuvent �tre des images, des textes, des formes g�om�triques ...

set c .frame.c
	canvas $c -width $glob(width) -height $glob(height) -bg $glob(bgcolor) -highlightbackground $glob(bgcolor)
pack $c -expand true 

# on cr�e une frame en bas en avant-dernier ligne avec
#   le score affich� sous forme de t�tes (bien passable mal)
#   un bouton 'continuer'

frame .bframe -bg $glob(bgcolor)
pack .bframe -side top -expand true

  image create photo pbien -file [file join sysdata pbien.gif] 
  image create photo ppass -file [file join sysdata ppass.gif]
  image create photo pmal -file [file join sysdata pmal.gif]
  image create photo pneutre -file [file join sysdata pneutre.gif]

for {set i 1} {$i <= $nbquestions} {incr i 1} {
  label .bframe.lab$i -bg $glob(bgcolor) -width 4
  grid .bframe.lab$i -column [expr $i -1] -row 0 -sticky e
  .bframe.lab$i configure -image pneutre -width 80
}

button .bframe.quitter_minus -image \
	[image create photo -file [file join sysdata quitter_minus.gif]] \
	-command exit
grid .bframe.quitter_minus -column [expr $nbquestions + 1] -padx 5 -row 0

# on cr�e une frame sur la derni�re ligne avec
#   une �tiquette de consigne/controle de la r�ponse (2e ligne)
frame .cframe -bg $glob(bgcolor)
pack .cframe -side bottom -expand true
label .cframe.control -bg $glob(bgcolor) -font 12x24
pack .cframe.control

# variable servant � changer le type d'exercice (codage ou d�codage)
global bascule
set bascule 1

##############################################
# ###
# lire le fichier tableau de jeu
# ###
proc lire_tableau {} {
  global didacticiel taillerect imgperso globcol globrow img1arr img2arr etiq1 etiq2
  set f [open [file join sysdata/4 tableau.jeu] "r"]
  set taillerect [gets $f]
  close $f 
  set imgperso ${didacticiel}_logo.gif

  set globrow 26 ;#ident de ligne = lettre
  for {set i 1} {$i <= $globrow} {incr i 1} {
    set lettre [string index "ABCDEFGHIJKLMNOPQRSTUVWXYZ" [expr $i -1]]
    set img1arr($i) let_$lettre.gif
    set etiq1($i) $lettre
  }
  set globcol 9 ;#ident de colonne = chiffre
  for {set i 1} {$i <= $globcol} {incr i 1} {
    set img2arr($i) chif_$i.gif
    set etiq2($i) $i
  }
}

####
# permuter les images
####
proc permuter_images {} {
  global imgperso difficulte globcol globrow nbcol nbrow img1arr img2arr etiq1 etiq2
  for {set i 1} {$i <= $globcol} {incr i 1} {
    set k [expr int(rand()*$globcol)+1]
    set imgswap $img2arr($i)
    set img2arr($i) $img2arr($k)
    set img2arr($k) $imgswap 
    set etiqswap $etiq2($i)
    set etiq2($i) $etiq2($k)
    set etiq2($k) $etiqswap
  }
  for {set i 1} {$i <= $globrow} {incr i 1} {
    set k [expr int(rand()*$globrow)+1]
    set imgswap $img1arr($i)
    set img1arr($i) $img1arr($k)
    set img1arr($k) $imgswap
    set etiqswap $etiq1($i)
    set etiq1($i) $etiq1($k)
    set etiq1($k) $etiqswap
  }
  
  #instancier nbcol et nbrow
  set nbrow 3
  set nbcol 4
  if {$nbcol> $globcol} {
    set nbcol $globcol
  }
  if {$nbrow> $globrow} {
    set nbrow $globrow
  }
}

# ##
# sauver trace-�l�ve et parcours
# ##
proc sauver_trace_parcours {} {
  global glob didacticiel difficulte nbessais nbquestions nbjustes heure_debut

  ## trace
  # utilisateur/classe/date/dur�e/didacticiel/niveau/difficulte/version/nbjustes/nbquestions
  set eleve [lindex [split $glob(trace_user) /] end]
    set heure_fin [clock seconds]
    set duree [expr $heure_fin-$heure_debut]
    set date_heure [clock format [clock seconds] -format "%A %d %B %Y %H:%M"]
    set trace_eleve "\{$eleve\} classe \{$date_heure\} $duree \{$didacticiel 4 $difficulte\} $glob(version)"
  lappend trace_eleve "$nbjustes $nbquestions"

  set f [open [file join $glob(trace_user).log] "a+"]
  puts $f $trace_eleve
  close $f

  ## parcours
  # bug Tcl/Tk ? Ne peut utiliser w+. Ouvrir r puis w et renommer
  #lire les parcours en lecture
  set lt_array($didacticiel) "-1 -1 -1 -1 -1" ;#sentinelle
  set f [open [file join $glob(trace_user).level] "r"]
  while { ! [eof $f] } {
    set line [gets $f]
    set lt_array([lindex $line 0]) [lrange $line 1 end]
  }
  close $f

  set parcours [lindex $lt_array($didacticiel) 4]

  #parcours suffisant ? Tol�rance : 0 ou 1 erreur
  if { $parcours < 4 && $nbjustes >= 5 } {
    set lt_array($didacticiel) [lreplace $lt_array($didacticiel) 4 4 4]
    #sauver dans fichier temporaire
    set f [open [file join $glob(trace_user).tmp] "w"]
    foreach local_didacticiel [array names lt_array] {
      puts $f "$local_didacticiel $lt_array($local_didacticiel)"
    }
    close $f
    #renommer
    file rename -force $glob(trace_user).tmp $glob(trace_user).level 
  }
} ;# sauver_trace_parcours


# proc�dure appel�e depuis 'Itemstopdrag' pour mettre en surbrillance les cellules
# � l'issue de la correction

proc recommence {c} {
  global glob didacticiel difficulte nbquestions nbjustes iquestion nbessai heure_debut
  set nbessai 0
  set iquestion 0
  set nbquestions 8
  set nbjustes 0

  wm title . "$didacticiel $difficulte"
  for {set i 1} {$i <= $nbquestions} {incr i 1} {
    .bframe.lab$i configure -image pneutre -width 80
  }
    set heure_debut [clock seconds]

  main $c
}

##########################################################"
proc highlight { c indice1 indice2} {
  for {set i 0} {$i < [expr $indice1]} {incr i 1} {
    $c itemconfigure rect$i$indice2 -fill pink
  }
  for {set i 0} {$i < [expr $indice2]} {incr i 1} {
    $c itemconfigure rect$indice1$i -fill pink
  }
  $c itemconfigure rect$indice1$indice2 -fill purple
}

########################################################"
# proc�dure utilis�e dans l'exercice de codage
proc coder_case {c} {
#########################################""""
  global glob didacticiel indice1 indice2 etiq1 etiq2 nbessai nbjustes taillerect iquestion nbquestions
  set str [string toupper [.cframe.text1 get]]
  if {[string length $str]==2} {
    incr nbessai
    if {[string compare $str $etiq1($indice1)$etiq2($indice2)]==0 || [string compare $str $etiq2($indice2)$etiq1($indice1)]==0 } {
      set str "[mc win]"
      catch {$c delete etiq}
      incr nbjustes

      $c create text 60 60 -text $str -font {Arial 12} -tags etiq
      if { $nbessai <2 } {
        .bframe.lab$iquestion configure -image pbien -width 80
      } else {
        .bframe.lab$iquestion configure -image ppass -width 80
      }
      highlight $c $indice2 $indice1
      if {$iquestion >= $nbquestions} {
        .bframe.quitter_minus configure -state disable
        sauver_trace_parcours
        .cframe.control configure -text "[mc score] $nbjustes/$nbquestions"
        grid .cframe.control -padx 64 
        catch {destroy .cframe.text1}
        catch {$c delete etiq}
        $c create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
          -image [image create photo -file [file join sysdata sourire.png]]
        update
        after [expr $glob(attente)*1000]
        .bframe.quitter_minus configure -state normal
        return
      }

      $c itemconf [$c find withtag drag] -tag dead
      .cframe.control configure -text $str
      update
      after 2000
      main $c
    } else {
      # gestion des erreurs
      switch $nbessai {
        1 { set str "[mc redo]"
            catch {$c delete etiq}
            $c create text 60 60 -text $str -font {Arial 12} -tags etiq
            bell
            update
            after 1000
            .cframe.text1 delete 0 end 
          }
        2 { set str "[mc lose]" 
            catch {$c delete etiq}
            $c create text 60 60 -text $str -font {Arial 12} -tags etiq
            highlight $c $indice2 $indice1
            bell
            update
            after 1000
            .cframe.text1 delete 0 end 
            update
            after 1000
            .cframe.text1 insert 0 $etiq1($indice1)$etiq2($indice2)
            .cframe.control configure -text "[mc it_was]"
            .bframe.lab$iquestion configure -image pmal -width 80
            update
            after 2000
            if {$iquestion >= $nbquestions} {
              .bframe.quitter_minus configure -state disable
              sauver_trace_parcours
              .cframe.text1 delete 0 end 
              .cframe.control configure -text "[mc score] $nbjustes/$nbquestions"
              grid .cframe.control -padx 64 
              catch {destroy .cframe.text1}
              catch {$c delete etiq}
              $c create image [expr int($glob(width)/2)] [expr int($glob(height)/2)]\
                -image [image create photo -file [file join sysdata pleurer.png]]
              update
              after [expr $glob(attente)*1000]
              .bframe.quitter_minus configure -state normal
              return
            } else {
              main $c
            }
        }
      }
    }
  }
} ;# coder_case

#########################################################################################
#proc�dure principale, charg�e de reboucler, appel�e depuis le bouton 'continuer'

proc main {c} {
########################Initialisation de variables###############################
# nbcol : nombre de colonnes du tableau
# nbrow : nombre de lignes 
# taillerect : taille d'une cellule du tableau
# on peut modifier le nb de lignes ou de colonnes du tableau, en tenant compte de la taille de la fen�tre
# en jouant sur la taille des cellules, en redimensionnant les images, en cr�ant d'autres images
# si l'on cr�e d'autres lignes ou colonnes
# indice1 et indice2 : num�ro des habits tir�s au hasard


  global glob didacticiel nbcol nbrow taillerect indice1 indice2 img1arr img2arr imgperso etiq1 etiq2 bascule listeval iquestion nbessai nbquestions

  lire_tableau
  permuter_images
  set nbessai 0
  incr iquestion

  # on efface la grille

  $c delete all

####################Cr�ation du tableau###################################
# On utilise des rectangles pour dessiner les cases, on aurait pu utiliser des lignes
# De m�me, on aurait pu utiliser le 'grid manager' pour g�rer le tout, mais c'aurait �t� peut �tre
# un peu plus compliqu�. L'avantage des rectangles, c'est qu'ils peuvent constituer des objets
# ind�pendants, utile si on veut leur associer des comportements

  for {set i 0} {$i <= [expr $nbcol]} {incr i 1} {
     for {set j 0} {$j <= [expr $nbrow]} {incr j 1} {
       $c create rect [expr $glob(org) + $i*$taillerect] [expr $glob(org) + $j*$taillerect ]  [expr $glob(org) + ($i+1)*$taillerect]  [expr $glob(org) + ($j+1)*$taillerect]  -width 2 -fill gray -tags rect$i$j
     }
  }

##################### placement des images de la ligne 0 et de la colonne 0########

  for {set i 1} {$i <= [expr $nbcol]} {incr i 1} {
    $c create image \
	[expr $glob(org) + $i*$taillerect + int($taillerect/2)] \
	[expr $glob(org) + int($taillerect/2) ] \
	-image [image create photo -file [file join sysdata/4 $img2arr($i)]]
  }

  for {set i 1} {$i <= [expr $nbrow]} {incr i 1} {
    $c create image \
	[expr $glob(org) + int($taillerect/2) ] \
	[expr $glob(org) + $i*$taillerect + int($taillerect/2)] \
	-image [image create photo -file [file join sysdata/4 $img1arr($i)]]
  }

  # fond clignotant dans la case (0,0)
  $c create image [expr $glob(org) + int($taillerect/2)] [expr $glob(org) + int($taillerect/2)] \
	-image [image create photo -file [file join sysdata cellule00.gif]]

  $c itemconfigure rect00 -fill #ffff80
####################Cr�ation du personnage#####################
# Nous ne sommes pas limit�s � ce genre de situations : on peut tr�s bien cr�er
# des images repr�sentant des lettres ou des syllabes ...
# Initialisation de 2 variables al�atoires pour le choix de la tenue vestimentaire

  set indice1 [expr int(rand()*$nbrow) + 1]
  set indice2 [expr int(rand()*$nbcol) + 1]

# Pour cr�er le forme, on est oblig� d'utiliser une astuce, � cause d'un 
# d�faut de gestion des images dans tcl/tk.
# Normalement, on cr�e en m�moire une image et l'on 'superpose' dessus 3 fichiers images 
# dans un format .gif ou .png pour b�n�ficier de la transparence, de mani�re � obtenir un seul objet
# Dans notre cas, on cr�era 3 images diff�rentes que l'on superpose, 
# mais on devra bouger les 3 images pour d�placer le personnage.

# A chacune des 3 images, on associe le tag 'lap' permettant de les reconna�tre plus tard
# pour les d�placer. A la derni�re image, on rajoute le tag 'drag', qui permettra de recevoir
# les �v�nements pour le d�placement

  image create photo forme
  $c create image 60 60 -image forme -tags drag
  forme read [file join $didacticiel $imgperso]
  $c addtag lap withtag drag

#################################################################
catch {destroy .cframe.text1}
###################################################################

# on place les widgets en fonction de l'exercice codage ou d�codage
  if {$bascule==1} {
    .cframe.control configure -text "[mc move_it] $etiq1($indice1) $etiq2($indice2)"
    grid .cframe.control -column 0 -row 0 -padx 80 
  } else {
    foreach i [$c find withtag lap] {
      $c coords $i \
	[expr $indice2*$taillerect + int($taillerect/2)] \
	[expr $indice1*$taillerect + int($taillerect/2)]
    }
    $c itemconf drag -tag dead
    .cframe.control configure -text "[mc where_is_it]"
    grid .cframe.control -column 1 -row 0 -padx 10 
    entry .cframe.text1 -relief sunken -bd 2 -width 6 -font {Arial 14}
    grid .cframe.text1 -column 2 -row 0
    focus .cframe.text1
  }

#####################################Gestion des �v�nements############################
# On associe la gestion des d�placements avec l'item portant le tag 'drag'
# c'est � dire le personnage (ou plus exactement la troisi�me image du personnage
# les 2 autres devant �tre synchronis�es)
# %x %y r�cup�rent la position de la souris.  

  $c bind drag <ButtonRelease-1> "itemStopDrag $c %x %y"
  $c bind drag <1> "itemStartDrag $c %x %y"
  $c bind drag <B1-Motion> "itemDrag $c %x %y"
  if {$bascule==0} {
    bind .cframe.text1 <KeyRelease> "coder_case $c"
    #bind .cframe.text1 <Return> "coder_case $c"
  }

  set bascule [expr 1 - $bascule]

}

# les proc�dures suivantes ne sont appel�es que pour l'exercice de d�codage



# ############################Lorsque l'on clique sur le personnage (bouton appuy�)
# On capture la position de la souris dans les variables lastX et lastY
# de m�me que les coordonn�es du personnage 'sourcecoord'

proc itemStartDrag {c x y} {
  global lastX lastY sourcecoord
  set sourcecoord [$c coords current]
  set lastX [$c canvasx $x]
  set lastY [$c canvasy $y]
  # R�initialisation du texte qui affiche 'vous �tes sur'
  $c delete etiq
  . config -cursor fleur
}

# #########################On rel�che la souris.
# C'est ici que l'on fait le traitement et l'analyse de la position du personnage
# Lorsque le traitement sera plus importants, on pourra cr�er une ou plusieurs proc�dures
# suppl�mentaires pour g�rer les essais, les erreurs, le rebouclage etc..

proc itemStopDrag {c x y} {
  global glob taillerect nbrow nbcol sourcecoord indice1 indice2 nbjustes nbquestions iquestion nbessai listeval

  incr nbessai
  . config -cursor left_ptr
  set coord [$c coords current]

  # On calcule le num�ro de la case o� se trouve le personnage (xc et yc)
  set xc [expr int([lindex $coord 0]/$taillerect)]
  set yc [expr int([lindex $coord 1]/$taillerect)]
  # On teste si le personnage est dans une case valide
  if {$xc>0 && $xc<=$nbcol && $yc>0 && $yc<=$nbrow} {
    # Si oui, on d�place le personnage
    foreach i [$c find withtag lap] { 
      $c coords $i \
	[expr $xc*$taillerect + int($taillerect/2)] \
	[expr $yc*$taillerect + int($taillerect/2)]
    }
      if {$xc==$indice2 && $yc==$indice1} {
        set str "[mc win]"
        incr nbjustes
        if { $nbessai <2 } {
          .bframe.lab$iquestion configure -image pbien -width 80
        } else {
          .bframe.lab$iquestion configure -image ppass -width 80
        }
        if {$iquestion >= $nbquestions} {
          sauver_trace_parcours
          .bframe.quitter_minus configure -state normal
         .cframe.control configure -text "[mc score] $nbjustes/$nbquestions."
          grid .cframe.control -padx 64 
          return
        }
        highlight $c $indice2 $indice1
        $c itemconf current -tag dead
        #.bframe.b1 configure -state normal
        $c create text 60 60 -text $str -font {Arial 12} -tags etiq
        update
        after 2000
        main $c
        return
      } else {
        # gestion des erreurs 
	switch $nbessai {
          1 { $c create text 60 60 -text [mc redo] -font {Arial 12} -tags etiq
              bell
            }
          2 {
              # On positionne correctement le personnage
              foreach i [$c find withtag lap] {
                $c coords $i \
			[expr $xc*$taillerect + int($taillerect/2)] \
			[expr $yc*$taillerect + int($taillerect/2)]
              }
              # On met en surbrillance les cellules
              highlight $c $indice2 $indice1
              set str "[mc lose]"
              $c create text 60 60 -text $str -font {Arial 12} -tags etiq
              .bframe.lab$iquestion configure -image pmal -width 80
              bell
              update
              after 1000
              # apr�s une seconde,
              # on met une croix dans la mauvaise case qui a �t� choisie
              $c create line \
		[expr $glob(org) + $xc*$taillerect] \
		[expr $glob(org) + $yc*$taillerect] \
		[expr $glob(org) + ($xc + 1)*$taillerect] \
		[expr $glob(org) + ($yc + 1)*$taillerect]
              $c create line \
		[expr $glob(org) + ($xc + 1)*$taillerect] \
		[expr $glob(org) + ($yc)*$taillerect] \
		[expr $glob(org) + $xc*$taillerect] \
		[expr $glob(org) + ($yc + 1)*$taillerect]
              $c itemconf current -tag dead
              update
              after 2000
              #.bframe.b1 configure -state normal
              .bframe.lab$iquestion configure -image pmal -width 80
              main $c
              return
	    }
        } ;# fin switch
      } ;#fin else
    } else {
      # Si non, on le renvoie � sa place                 
      foreach i [$c find withtag lap] {
        $c coords $i [lindex $sourcecoord 0] [lindex $sourcecoord 1]
      }
    }
}


# ##################D�placement du personnage, on d�place g�re les coordonn�es, toujours avec lastX et lastY
proc itemDrag {c x y} {
  global lastX lastY
  set x [$c canvasx $x]
  set y [$c canvasy $y]
  # On deplace tous les objets poss�dant le tag 'lap', c'est � dire les 3 images du personnage
  $c move lap [expr $x-$lastX] [expr $y-$lastY]
  set lastX $x
  set lastY $y
}

##################################################################"
  # Relire le nom r�gl� de l'utilisateur sous windows
  if {$glob(platform) == "windows"} {
    catch {set f [open [file join $glob(home_tableaux) reglages trace_user] "r"]}
    gets $f glob(trace_user)
    close $f
  }

recommence $c
