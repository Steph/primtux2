(function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.nombresympathique = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre;
var aNombres,aIndiceAleatoire,cnt,aJeton,tuple,soluce,champReponse,consigne,conseil,gExpression;
var creerJeton  = function (valeur) {
    var gJeton = cnt.paper.group(),
        fond = cnt.paper.circle(60).stroke({width:5,color:"#aaa"}),
        label = cnt.paper.plain(""+valeur).attr({"font-size":"32px"});
    gJeton.add(fond);
    gJeton.add(label);
    label.fill("#000");
    //label.leading(9.5);
    label.center(30,30);
    gJeton.fill("#FF6DDD");
    gJeton.valeur = valeur;
    gJeton.attr({cursor:"pointer"});
    return gJeton;
};  

// Référencer les ressources de l'exercice (image, son)

exo.oRessources = { 
    illustration : "nombresympathique/images/illustration.png"
};

// Options par défaut de l'exercice (définir au moins totalQuestion et tempsExo )

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:5,
        totalEssai:1,
        tempsExo:0,
        temps_expo:100,
        nbreTerme:5,
        sommeSympa:50,
        effacement:false,
        deplacement:true
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
    //Creation des donnees
    var i,j,k,sum,aNombreSympa1,aNombreSympa2;
    //
    sum = exo.options.sommeSympa;
    if(exo.options.nbreTerme==3)
    {
        aNombreSympa1 = generateNbreSympa(sum);
        aNombres = generateTriplet(aNombreSympa1,2,sum-2);
        aNombres.sort(function(){return Math.floor(Math.random()*3)-1;});
        
    }
    
    if(exo.options.nbreTerme==5)
    {
        aNombreSympa1 = generateNbreSympa(sum);
        aNombreSympa2 = generateNbreSympa(10);
        aNombres = generateQuintuplet(aNombreSympa1,aNombreSympa2,2,sum-2);
        aNombres.sort(function(){return Math.floor(Math.random()*3)-1;});
    }
    
    //Retoune un tableau de tous les couples 
    //de nombres sympas dont la somme <= total
    //total etant obligatoirement un multiple de 10
    function generateNbreSympa(total)
    {
        var i, j, aTotal = [], nombres = [];
        
        for (i=1;i<=total/10;i++)
        {
            aTotal.push(i*10);
        }
        
        for(j=0;j<aTotal.length;j++)
        {
            for (i=0;i<=aTotal[j]/2;i++)
            {
                if(i%10 !== 0 && i%10 != 1 && i%10 != 9)
                {
                    nombres.push([aTotal[j]-i,i]);
                }
            }
        }
        nombres.sort(function(){return Math.floor(Math.random()*3)-1;});
        return nombres;
    }
    
    function generateTriplet(nbreSympa,min,max)
    {
        var nombres =[];
        for (var i=0;i<nbreSympa.length;i++)
        {
            var nA=nbreSympa[i][0],
                nB=nbreSympa[i][1],
                nC;
            while(true)
            {
                nC= min + Math.floor(Math.random()*(max-min+1));
                if((nC+nA)%10 !== 0 && (nC+nB)%10 !== 0 && nC%10 !== 0 && nC%10 != 1 && nC%10 != 9)
                {
                    break;
                }
            }
            nombres.push([nA,nB,nC]);
        }
        return nombres;
    }
    
    function generateQuintuplet(nbreSympa1,nbreSympa2,min,max)
    {
        var nombres = [];
        for (var i=0;i<nbreSympa1.length;i++)
        {
            var nA=nbreSympa1[i][0],
                nB=nbreSympa1[i][1],
                nC,nD,nE;
            while(true)
            {
                //nbreSympa2.sort(function(){return Math.floor(Math.random()*3)-1;});
                util.shuffleArray(nbreSympa2);
                nC = nbreSympa2[0][0];
                nD = nbreSympa2[0][1];
                if((nA+nC)%10 !== 0 && (nA+nD)%10 !== 0 && (nB+nC)%10 !== 0 && (nB+nD)%10 !== 0 )
                {
                    break;
                }
            }
            while(true)
            {
                nE= min + Math.floor(Math.random()*(max-min+1));
                if(
                        (nA+nE)%10 !== 0 
                    &&  (nB+nE)%10 !== 0 
                    &&  (nC+nE)%10 !== 0 
                    &&  (nD+nE)%10 !== 0 
                    &&  nE%10 !== 0 
                    &&  nE%10 != 1 
                    &&  nE%10 != 9
                )
                {
                    break;
                }
            }
            nombres.push([nA,nB,nC,nD,nE]);
        }
        return nombres;
    }
};

//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Les nombres sympathiques");
    //exo.blocConsigneGenerale.html("")
    var illustr = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.append(illustr);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    exo.keyboard.config({
        arrow : "disabled"
    });
    
    cnt = disp.createSvgJsContainer(735,450);
    exo.blocAnimation.append(cnt);
    var nbreTerme = exo.options.nbreTerme;
    tuple = aNombres[exo.indiceQuestion];
    soluce=0;
    for (var i=0;i<tuple.length;i++) {
        soluce+=tuple[i];
        //console.log("soluce",soluce)
    }
    if (exo.options.nbreTerme==3) {
        aIndiceAleatoire = util.shuffleArray([0,1,2]);
    } else {
        aIndiceAleatoire = util.shuffleArray([0,1,2,3,4]);
    }
    
    //la consigne
    var texte;
    if (exo.options.effacement) {
        texte = "Observe bien le calcul. Il va disparaître.";
    } else {
        texte = "Donne le résultat du calcul.";
    }
    consigne = cnt.paper.plain(texte).attr({"font-size":"24px","kerning":1.5});
    consigne.center(735/2,30);
    
    var empreinteVide, carteDeplace, aEmpreinte=[];
    
    //On construit l'expression a resoudre
    gExpression = cnt.paper.group();
    // les empreintes
    var empreinte;
    for ( i=0 ; i < nbreTerme ; i++) {
        empreinte = cnt.paper.circle(60).attr({stroke:"#ccc",fill:"#fff"});
        empreinte.move(120*i,0);
        empreinte.vide =false;
        aEmpreinte.push(empreinte.bbox().x);
        gExpression.add(empreinte);
    }
    
    // les signes
    var signe;
    for (var j=0 ; j < nbreTerme ; j++) {
        texte = j==nbreTerme-1 ? "=" : "+";
        signe = cnt.paper.plain(texte).attr({"font-size":"38px"});
        //signe.leading(9.5);
        signe.center(90+(120*j),30,true);
        gExpression.add(signe);
    }
    if (exo.options.effacement) {
        signe = cnt.paper.plain(" ?").attr({"font-size":"38px"});
        //signe.leading(9.5);
        signe.center(90+(120*(j-1)+30),30,true);
        gExpression.add(signe);
    }
    
    // les nombres deplaçables
    aJeton = [];
    var indexVide,indice,valeur,jeton;
    for (var k=0 ; k < nbreTerme ; k++) {
        indice =aIndiceAleatoire[k];
        valeur =  tuple[indice];
        jeton = creerJeton(valeur);
        jeton.pIndex = k;
        jeton.move(120*k,0);
        if (exo.options.deplacement) {
           jeton.touchDraggable({minY:0,maxY:60});
        }
        
        jeton.dragstart = gestionDragStart;
        jeton.dragmove = gestionDragMove;
        jeton.dragend = gestionDragEnd;
        aJeton.push(jeton);
        gExpression.add(jeton);
    }
    
    // on centre l'expression
    var x;
    if (exo.options.effacement === true) {
        x = (735 - gExpression.bbox().width)/2;
        gExpression.move(x,100);
    } else{
        x = (735 - gExpression.bbox().width)/2 - 60;
        gExpression.move(x,100);
    }
    
    // le champ réponse
    champReponse = disp.createTextField(exo,3);
    exo.blocAnimation.append(champReponse);
    if (exo.options.effacement === false) {
        champReponse.css({
            left:gExpression.bbox().x+gExpression.bbox().width+20,
            top : gExpression.bbox().y+5,
        }); 
    } else {
        champReponse.css({
            left:735/2 - champReponse.width()/2,
            top : 100,
            visibility:'hidden'
        });
    }
    champReponse.focus();
    
    // l'aide
    if (exo.options.deplacement === true) {
        texte="Tu peux déplacer les nombres pour rendre le calcul plus facile.";
    }
    else{
        texte="Regroupe, \"de tête\", les nombres pour rendre le calcul plus facile.";
    }
    conseil = cnt.paper.plain(texte).attr({"font-size":"20px","kerning":1.5});
    conseil.center(735/2,180);
    
    //le bouton valider
    if (exo.options.effacement === true) {
        exo.btnValider.hide();
    }
    
    //on declenche l'effacement le cas echeant
    if (exo.options.effacement === true) {
        var animation = anim.creerDemon(gExpression,effacerCalcul,exo.options.temps_expo*100,1);
        animation.start();
    }
   
    
    
    // fonctios utiles

    function gestionDragStart(e) {
        this.front();indexVide=this.pIndex;
    }
    
    function gestionDragMove(delta,e){
        for (var j = 0 ; j < aJeton.length ; j++) {
            if (this.intersect(aJeton[j]) && this != aJeton[j]) {
                var dx=aEmpreinte[indexVide];
                aJeton[j].x(dx);
                this.pIndex= aJeton[j].pIndex;
                aJeton[j].pIndex = indexVide;
                indexVide=this.pIndex;
                //console.log(indexVide);
                break;
            }
        }
        
    }
    
    function gestionDragEnd(e){
        var dx = aEmpreinte[indexVide];
        this.x(dx);
    }
    
    function effacerCalcul(index) {
        this.hide();
        conseil.hide();
        champReponse.css({visibility:"visible"}).focus();
        consigne.text("Donne le résultat.");
        exo.btnValider.show();
    }
    
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var repEleve = champReponse.val();
    if (repEleve === "") {
        return "rien";
    }
    else if (Number(repEleve) == soluce) {
        return "juste";
    }
    else {
        return "faux";
    }
 
    
};

// Correction (peut rester vide)

exo.corriger = function() {
    exo.cacherBtnSuite();
    var i,index,jeton,lien,
        aCorrection = [],
        somme=0,
        gCorrection = cnt.paper.group();
        
    
    if (exo.options.effacement) {
        gExpression.show();
        x = (735 - gExpression.bbox().width)/2 - 40;
        gExpression.move(x,100);
        champReponse.css({
            left:gExpression.bbox().x+gExpression.bbox().width-20,
            top : gExpression.bbox().y+5,
        }); 
    }
      
    correction = disp.createCorrectionLabel(soluce);
    exo.blocAnimation.append(correction);
    correction.position({
        my:"left center",
        at:"right center",
        of:champReponse,
        offset:"20px 0px"
    });
    
    barre = disp.drawBar(champReponse);  
    exo.blocAnimation.append(barre);
    
    if(exo.options.deplacement){
        for( i=0; i<aJeton.length; i++){
            aJeton[i].fixed();
        }
    }
    conseil.hide();
    gCorrection.move(100,190).scale(0.7);
    
    var animCorrection;
    if (exo.options.nbreTerme == 3) {
       animCorrection = anim.creerDemon(aJeton,animerA,1000,8);
    } else {
        animCorrection = anim.creerDemon(aJeton,animerB,1200,14);
    }
    
    animCorrection.start();
    
    // la correction pour une expression a 3 termes
    function animerA(etape) {
        exo.btnSuite.hide();
        if ( etape <= 2 ){
            index = aIndiceAleatoire.indexOf(etape-1);
            this[index].fill({color:"#FFD100"});
        }
        else if ( etape <= 4 ) {
            i = etape-3;
            index = aIndiceAleatoire.indexOf(i);
            jeton = creerJeton(this[index].valeur).fill("#FFD100");
            jeton.move(120*i,0);
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            somme+=jeton.valeur;    
        }
        else if (etape == 5) {
            lien = relier(aCorrection[0],aCorrection[1]);
            gCorrection.add(lien);
            
            jeton = creerJeton(somme).fill("#ccc");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35);    
        }
        else if ( etape == 6 ) {
            index = aIndiceAleatoire.indexOf(2);
            aJeton[index].fill("#C1FF07");  
        }
        else if ( etape == 7 ) {
            index = aIndiceAleatoire.indexOf(2);
            this[index].fill("#C1FF07");
            jeton = creerJeton(this[index].valeur).fill("#C1FF07");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.move(240,lien.bbox().y+lien.bbox().height+5);
            somme+=jeton.valeur;    
        }
        else if ( etape == 8 ) {
            lien = relier(aCorrection[2],aCorrection[3]);
            gCorrection.add(lien);
            
            jeton = creerJeton(somme).fill("#ccc");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35) ;
            exo.afficherBtnSuite();
        }
    }
    
    //la correction pour une expression à 5 termes
    function animerB(etape) {
        exo.btnSuite.hide();
        if ( etape <= 2 ){
            index = aIndiceAleatoire.indexOf(etape-1);
            this[index].fill({color:"#FFD100"});
        }
        else if ( etape <= 4 ) {
            i = etape-3;
            index = aIndiceAleatoire.indexOf(i);
            jeton = creerJeton(this[index].valeur).fill("#FFD100");
            jeton.move(120*i,0);
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            somme+=jeton.valeur;    
        }
        else if (etape == 5) {
            lien = relier(aCorrection[0],aCorrection[1]);
            gCorrection.add(lien);
            
            jeton = creerJeton(somme).fill("#fff");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35);    
        }
        else if ( etape <= 7 ){
            i=etape-4;
            index = aIndiceAleatoire.indexOf(i);
            this[index].fill({color:"#C1FF07"});
        }
        else if ( etape <= 9 ){
            i = etape-6;
            index = aIndiceAleatoire.indexOf(i);
            jeton = creerJeton(this[index].valeur).fill("#C1FF07");
            jeton.move(120*i,0);
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            somme+=jeton.valeur;    
        }
        else if (etape == 10) {
            lien = relier(aCorrection[3],aCorrection[4]);
            gCorrection.add(lien);
            
            jeton = creerJeton(aCorrection[3].valeur+aCorrection[4].valeur).fill("#fff");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35);    
        }
        else if (etape == 11) {
            lien = relier(aCorrection[2],aCorrection[5]);
            gCorrection.add(lien);
            
            jeton = creerJeton(somme).fill("#fff");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35);    
        }
        if ( etape == 12 ){
            index = aIndiceAleatoire.indexOf(4);
            this[index].fill({color:"#2CD3FF"});
        }
        else if ( etape == 13 ) {
            index = aIndiceAleatoire.indexOf(4);
            jeton = creerJeton(this[index].valeur).fill("#2CD3FF");
            posY = aCorrection[6].bbox().y;
            jeton.move(120*4,posY);
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            somme+=jeton.valeur;    
        }
        else if (etape == 14) {
            lien = relier(aCorrection[6],aCorrection[7]);
            gCorrection.add(lien);
            
            jeton = creerJeton(somme).fill("#fff");
            aCorrection.push(jeton);
            gCorrection.add(jeton);
            jeton.center(lien.bbox().x+lien.bbox().width/2,lien.bbox().y+lien.bbox().height+35);
            exo.afficherBtnSuite();
        }
    }
    
    function relier(A,B) {
        var bboxA = A.bbox(),
            bboxB = B.bbox(),
            x2 = (bboxB.x-bboxA.x)/2,
            y2 = 30,
            x3 = bboxB.x-bboxA.x,
            y3 = 0;
    
        var lien = cnt.paper.polyline([[0,0],[x2,y2],[x3,y3]]).fill('none').stroke({width:3,color:"#aaa"});
        
        lien.move(bboxA.x+bboxA.width/2,bboxA.y+bboxA.height+5);
        
        return lien;
    }
    
    
    
};

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle;
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"nbreTerme",
        texte:"Nombre de termes du calcul : ",
        aLabel:["3","5"],
        aValeur:[3,5]
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"sommeSympa",
        texte:"La somme des termes 'sympathiques' est :",
        aLabel:[
            "inférieure ou égale à 20",
            "inférieure ou égale à 30",
            "inférieure ou égale à 40",
            "inférieure ou égale à 50",
            "inférieure ou égale à 60",
            "inférieure ou égale à 70",
            "inférieure ou égale à 80",
            "inférieure ou égale à 90",
            "inférieure ou égale à 100"
        ],
        aValeur:[20,30,40,50,60,70,80,90,100]
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"deplacement",
        texte:"Les nombres peuvent être déplaçés : ",
        aLabel:["oui","non"],
        aValeur:[true,false]
    });
    exo.blocParametre.append(controle);
    
    controle = disp.createOptControl(exo,{
        type:"radio",
        nom:"effacement",
        texte:"Le calcul doit disparaître : ",
        aLabel:["oui","non"],
        aValeur:[true,false]
    });
    exo.blocParametre.append(controle);
    
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"temps_expo",
        texte:"Effacement au bout de (en dixièmes de secondes)  : "
    });
    exo.blocParametre.append(controle);
    
    //
    controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:"Temps pour réaliser l'exercice (en secondes)  : "
    });
    exo.blocParametre.append(controle);

};

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
};
return clc;
}(CLC));