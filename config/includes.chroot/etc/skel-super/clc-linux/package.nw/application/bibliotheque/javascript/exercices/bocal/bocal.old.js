var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.bocal = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var aNombre = [];
var fondA, fondB, poisson, champReponse, k, vitesseBulle, vitessePoisson;
var bulle = [];
var etiquette = [];

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = {
	txt:"bocal/textes/bocal_fr.json",
	poisson:"bocal/images/Poisson.png",
	bulle:"bocal/images/bulle_02.png",
    illustration:"bocal/images/illustration.png",
	fond:"bocal/images/fond.jpg"
}

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
		totalQuestion:2,
		totalEssai:1,
		temps_question:0,
		nBulleMax:5,
		plagePremierNombre:"2-9",
		plageAutreNombre:"12-19",
		choixVitesse:2
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
	//vitesse de la bulle
	switch(exo.options.choixVitesse) {
		case 1:
			vitesseBulle = 4500;
			vitessePoisson = 3500;
			break;
		case 2:
			vitesseBulle = 3000;
			vitessePoisson = 2000;
			break;
		case 3:
			vitesseBulle = 2500;
			vitessePoisson = 1500;
			break;
	}
	//les nombres de la premiere bulle
	var premierNombre = [];
	var aTemp = [];
	var dif, i, j;
	premierNombre = util.getArrayNombre(exo.options.plagePremierNombre);
	dif=exo.options.totalQuestion-premierNombre.length;
	if(dif>0) {
		for (i=0;i<dif;i++)	{
			premierNombre.push(premierNombre[i]);
		}
	}
	util.shuffleArray(premierNombre);
	
	//les nombres des autres bulles
	var autreNombre = [];
	autreNombre = util.getArrayNombre(exo.options.plageAutreNombre);
	util.shuffleArray(autreNombre);
	dif=exo.options.nBulleMax-1-autreNombre.length;
	if(dif>0) {
		for (i=0;i<dif;i++) {
			autreNombre.push(autreNombre[i]);
		}
	}
	//le tableau qui contient tous les nombres (pemieres bulles + autres bulles)
	for(i=0;i<exo.options.totalQuestion;i++) {
		aTemp.push(premierNombre[i]);
		util.shuffleArray(autreNombre);
		if(dif<0) {
			for(j=0;j<exo.options.nBulleMax-1;j++) {
				aTemp.push(autreNombre[j]);
			}
		}
		else {
			aTemp = aTemp.concat(autreNombre);
		}
		var bonneReponse = 0;
		for (k=0;k<aTemp.length;k++) {
			bonneReponse += Number(aTemp[k]);
		}
		aTemp.push(bonneReponse);
		aNombre.push(aTemp);
		aTemp = [];
	}
	console.log(aNombre);
//	for(i=0;i<exo.options.nBulleMax;i++) {
//		aNombre[i]=i;
//	}

}

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneGenerale);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
	
	exo.keyboard.config({
		numeric:"enabled",
		arrow:"disabled",
		large:"enabled",
	});
	
	exo.btnValider.hide();
	
	/*
	fondA = disp.createEmptySprite();
	fondB = disp.createEmptySprite();
	fondA.css({"background-color":"#CCCBFF",width:735,height:305,left:0,top:0});
	fondB.css({"background-color":"#E5E5CB",width:735,height:145,left:0,top:305});
	exo.blocAnimation.append(fondA);
	exo.blocAnimation.append(fondB);
	*/
	
	var fond = disp.createImageSprite(exo,"fond");
	exo.blocAnimation.append(fond);
	
	poisson = disp.createImageSprite(exo,"poisson");
	poisson.css({width:85,height:60,left:160,top:220});
	poisson.data("placement",0);
	exo.blocAnimation.append(poisson);
	
	var animPoisson = anim.creerDemon(poisson,bougerPoisson,2200,exo.options.nBulleMax);
	animPoisson.start();

	k=0; //indice de la bulle crachée
	
function bougerPoisson(etape){
		if (etape==1) {
			this.transition({x:"+="+330},vitessePoisson,'easeInOutSine',cracherBulle);			
		}
		if (etape%2==0) {
			poisson.transition({perspective: '200px',rotateY: '180deg'});
			//Retournement et direction gauche
			this.transition({x:"-="+330},vitessePoisson,'easeInOutSine',cracherBulle);
		} else if (etape%2==1 && (etape!=1)) {
			poisson.transition({perspective: '200px',rotateY: '0deg'});
			//retournement et direction droite
			this.transition({x:"+="+330},vitessePoisson,'easeInOutSine',cracherBulle);
		}
		
	}
	function cracherBulle(e){

		bulle[k] = disp.createImageSprite(exo,"bulle").css({"width":50,"height":50});
		console.log("Bulle numéro ",k)

		exo.blocAnimation.append(bulle[k]);
		etiquette[k] = disp.createTextLabel(String(aNombre[q][k])).css({"font-size":32});
		bulle[k].append(etiquette[k]);
		bulle[k].transition({scale:0.2});
		
		if (k%2==0) {
			bulle[k].position({
				my:"left center",
				at:"right center",
				of:poisson
			});
		} else {
			bulle[k].position({
				my:"left center",
				at:"left center",
				of:poisson
			});			
		}
		bulle[k].transition({scale:1,y:"-=50"},1000,'easeOutBack');
		exo.setTimeout(function(){
			etiquette[k].position({
				my:"center",
				at:"center",
				of:bulle[k]
			});
			//etiquette[k].transition({scale:0.2});
			k++;
		},100);
		if (k==(exo.options.nBulleMax-1)) {
			bulle[k].transition({y:"-=250"},vitesseBulle,'linear',afficherTxtField);
		} else {
			bulle[k].transition({y:"-=250"},vitesseBulle,'linear');			
		}
	}
	function afficherTxtField(e) {
		console.log("La bonne réponse est",aNombre[q][k]);
		champReponse = disp.createTextField(exo,5);
		exo.blocAnimation.append(champReponse);
		champReponse.position({
			my:"center center",
			at:"center center",
			of:exo.blocAnimation
		});
		champReponse.focus();
		exo.btnValider.show();
	}
}


// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
    if (champReponse.val() == "") {
       return "rien" 
    } else if( Number(champReponse.val()) == aNombre[q][k] ) {
        return "juste";
    } else {
        return "faux";
    }
}

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
    var correction = disp.createCorrectionLabel(aNombre[q][k]);
	var signe=[];
    exo.blocAnimation.append(correction)
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    })
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
	
	for (i=0;i<exo.options.nBulleMax;i++){
		var x = (i+1) * (735/(exo.options.nBulleMax+2));
//		bulle[i].css({left:x,top:400});
		var position = "left+"+x+" top+125";
		bulle[i].position({my:'center top',at:position,of:exo.blocAnimation});

		((i+1)==exo.options.nBulleMax) ? signe[i] = disp.createTextLabel("=") : signe[i] = disp.createTextLabel("+");
		
		x = x + (((i+2) * (735/(exo.options.nBulleMax+2)))-x)/2;
		position = "left+"+x+" top+125";
		exo.blocAnimation.append(signe[i]);
		signe[i].position({my:'center top',at:position,of:exo.blocAnimation}).css({"font-size":32});
	}
	
	var reponse = disp.createTextLabel(util.numToStr(aNombre[q][k]));
	exo.blocAnimation.append(reponse);
	x = (exo.options.nBulleMax+1)*(735/(exo.options.nBulleMax+2));
	reponse.css({left:x,top:125,"font-size":32});
	
	
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
//    var conteneur = disp.createOptConteneur();
//    exo.blocParametre.append(conteneur);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"totalQuestion",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"totalEssai",
        texte:exo.txt.option2
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"nBulleMax",
        texte:exo.txt.option3
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:128,
        taille:10,
        nom:"plagePremierNombre",
        texte:exo.txt.option4
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:128,
        taille:10,
        nom:"plageAutreNombre",
        texte:exo.txt.option5
    });
    exo.blocParametre.append(controle);
    
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"choixVitesse",
        texte:exo.txt.option6,
        aValeur:[1,2,3],
        aLabel:exo.txt.valeur6
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))