var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.template par clc.nom-de-votre-exercice
clc.basketmath = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.
// ex : var repEleve, soluce, champReponse, arNombre=[1,2,3];
var aPanneau,aFilet,aEtiquette,panneauClique,aExpression,aResultat,aSoluce,resEval;

// Définir les options par défaut de l'exercice
// (définir au moins totalQuestion, totalEssai, et tempsExo )
exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:10,
        totalEssai:1,
        tempsExo:180,
        cible:[1,2,3]
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Référencer les ressources de l'exercice (textes, image, son)
// exo.oRessources peut être soit un objet soit une fonction qui renvoie un objet
exo.oRessources = { 
    txt     :   "basketmath/textes/basketmath_fr.json",
    illustr :   "basketmath/images/basketmath.jpg",
    fond    :   "basketmath/images/fond.png",
    panneau :   "basketmath/images/panneau.png",
    ballon  :   "basketmath/images/ballon.png",
    filet   :   "basketmath/images/filet.png"
    // illustration : "template/images/illustration.png"
}

// Création des données de l'exercice (peut rester vide),
// exécutée a chaque fois que l'on commence ou recommence l'exercice
exo.creerDonnees = function() {
    var aCible= [500,1000,1500,2000,2500,3000,3500,4000,4500,5000];
    aExpression=[];aResultat=[],aSoluce=[];
    for(var i=0;i<aCible.length;i++){
        var sNumA = util.numToStr(aCible[i]);
        var sNumB = util.numToStr(aCible[i]/200);
        var sNumC = util.numToStr(aCible[i]/4);
        aExpression[i] = [ sNumA +" : 2",sNumB +" x 1 000", sNumC+" x 200" ];
        var aValeur = [aCible[i]/2 ,10*aCible[i]/2 , 100*aCible[i]/2 ];
        var alea = Math.floor(Math.random()*3);
        aSoluce[i]=alea;
        aResultat[i] = aValeur[alea] 
    }
}

//Création de la page titre : 3 éléments exo.blocTitre,
// exo.blocConsigneGenerale, exo.blocIllustration
exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigne);
    var illustration = disp.createImageSprite(exo,"illustr");
    exo.blocIllustration.html(illustration)
}

//Création de la page question, exécutée à chaque question,
// tous les éléments de la page doivent être ajoutés à exo.blocAnimation
exo.creerPageQuestion = function() {
    var vH,vV,vW,dX,dY,t,rot,g;
    //
    exo.btnValider.hide();
    //
    var fond = disp.createImageSprite(exo,"fond");
    exo.blocAnimation.append(fond);
    //
    var aPosPanneau = [83,293,493];
    aPanneau=[]
    for ( var i=0; i<3 ; i++ ){
        aPanneau[i] = disp.createImageSprite(exo,"panneau");
        aPanneau[i].addClass("clickable");
        aPanneau[i].data({index:i})
        aPanneau[i].css({left:aPosPanneau[i], top:50});
        exo.blocAnimation.append(aPanneau[i]);
        //aPanneau[i].on("mousedown.clc touchstart.clc",gererClickPanneau);
    }
    //
    var aPosFilet = [120.77,330.77,530.77];
    aFilet=[];
    for ( var i=0; i<3 ; i++ ){
        aFilet[i] = disp.createImageSprite(exo,"filet");
        aFilet[i].addClass("clickable");
        aFilet[i].data({index:i})
        aFilet[i].css({left:aPosFilet[i], top:130.5});
        exo.blocAnimation.append(aFilet[i]);
        //aFilet[i].on("mousedown.clc touchstart.clc",gererClickPanneau);
    }
    
    //
    var listeExpression = aExpression[exo.indiceQuestion];
    aEtiquette=[];
    for ( var i=0; i<3 ; i++ ){
        aEtiquette[i] = disp.createTextLabel(listeExpression[i]);
        aEtiquette[i].addClass("clickable");
        aEtiquette[i].data({index:i})
        exo.blocAnimation.append(aEtiquette[i]);
        aEtiquette[i].position({my:"center top",at:"center top+20",of:aPanneau[i]});
        //aEtiquette[i].on("mousedown.clc touchstart.clc",gererClickPanneau);
    }
    
    //
    var consigne = disp.createTextLabel("Clique sur l'opération dont le résultat est :");
    exo.blocAnimation.append(consigne);
    consigne.position({my:"center top",at:"center top+270",of:exo.blocAnimation});
    var sRes = util.numToStr(aResultat[exo.indiceQuestion])
    var lblResultat = disp.createTextLabel(sRes);
    exo.blocAnimation.append(lblResultat);
    lblResultat.css({fontSize:48,fontWeight:"bold"}).position({my:"center top",at:"center bottom",of:consigne});
    
    //
    var ballon = disp.createImageSprite(exo,"ballon");
    var imgBallon = ballon.children("img");
    exo.blocAnimation.append(ballon);
    ballon.position({my:"center center", at:"center bottom-50", of:exo.blocAnimation});
    
    $(".clickable").on("mousedown.clc touchstart.clc",gererClickPanneau);
    
    
    /* ***************************** */
    function gererClickPanneau(e){
        e.preventDefault();
        console.log(".clickable",$(".clickable"))
        $(".clickable").off("mousedown.clc touchstart.clc");
        panneauClique = $(e.delegateTarget);
        console.log(panneauClique.data().index);
        if (panneauClique.data().index == aSoluce[exo.indiceQuestion] ) {
            resEval = "juste";
        } else {
            resEval = "faux";
        }
        
        var animId;
        
        for ( var i=0; i<3 ; i++ ){
            aPanneau[i].off("click")
        }
        vV = 800;
        rot = "-=10deg";
        t = 0;
        g = 880;
        dY = ballon.position().top;
        dX = ballon.position().left+ballon.width()/2;
        vW = 25;
        if (panneauClique.data().index == 0 && resEval == "juste" ) {
            vH = -150;
        }
        else if (panneauClique.data().index == 2 && resEval == "juste" ) {
            vH = 150;
        }
        else if (panneauClique.data().index == 1 && resEval == "juste" ) {
            vH = 0;
            vV = 700;
        } else if (panneauClique.data().index == 0 && resEval == "faux" ) {
            vH = -200;
        }
        else if (panneauClique.data().index == 2 && resEval == "faux" ) {
            vH = 200;
        }
        else if (panneauClique.data().index == 1 && resEval == "faux" ) {
            vH = 70;
            vV = 750;
        }
        
        //reussirShoot();
        shooter();
        
        
    }
    
    function shooter(dt) {
        animId = requestAnimationFrame(shooter);
        //premiere partie de la trajectoire jusqu'au panier
        if ((ballon.position().top <= dY && g*t <= vV)
            || (ballon.position().top <= 80 && g*t > vV)
            )
        {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            var largeur = 80-vW*t;
            ballon.css({left:posX-ballon.width()/2,top:posY,width:largeur});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        //deuxième partie redescente à la verticale
        else if(ballon.position().top<250 && ballon.position().top > 80 && g*t > vV ){
            //on fait passer les filets devant
            for ( var i=0; i<3 ; i++ ){
                aFilet[i].css({zIndex:1000})
            }
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = -100*t + dX;
            ballon.css({top:posY});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        // arret de l'anim
        else if( ballon.position().top>=250 && g*t > vV){
            cancelAnimationFrame(animId);
            vV = 200;
            t = 0;
            g=440;
            vH *= 3/4;
            dY = ballon.position().top;
            dX = ballon.position().left;
            if (panneauClique.data().index == 0) {
                rebondirGauche()
            } else if (panneauClique.data().index == 2) {
                rebondirDroite()
            } if (panneauClique.data().index == 1) {
                rebondir();
            }
        }
    }
    
    function manquerShoot(dt) {
        vH=-200
        animId = requestAnimationFrame(reussirShoot);
        //premiere partie de la trajectoire jusqu'au panier
        if ((ballon.position().top <= dY && g*t <= vV)
            || (ballon.position().top <= 80 && g*t > vV)
            )
        {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            ballon.css({left:posX-ballon.width()/2,top:posY,width:dW});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        //deuxième partie redescente à la verticale
        else if(ballon.position().top<250 && ballon.position().top > 80 && g*t > vV ){
            //on fait passer les filets devant
            for ( var i=0; i<3 ; i++ ){
                aFilet[i].css({zIndex:1000})
            }
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = -100*t + dX;
            ballon.css({top:posY});
            imgBallon.css({rotate:rot});
            t+=1/62;
        }
        // arret de l'anim
        else if( ballon.position().top>=250 && g*t > vV){
            cancelAnimationFrame(animId);
            vV = 200;
            t = 0;
            g=440;
            vH *= 3/4;
            dY = ballon.position().top;
            dX = ballon.position().left;
            if (panneauClique.data().index == 0) {
                rebondirGauche()
            } else if (panneauClique.data().index == 2) {
                rebondirDroite()
            } if (panneauClique.data().index == 1) {
                rebondir();
            }
        }
    }
    
    function rebondirGauche(dt){
        animId = requestAnimationFrame(rebondirGauche);
        if (ballon.position().top<=dY && ballon.position().left >-ballon.width() ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            ballon.css({left:posX,top:posY});
            imgBallon.css({rotate:"-=1deg"})
            t+=1/31;
        } else if (ballon.position().left >-ballon.width() ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            vH *= 3/4;
            dX = ballon.position().left;
            ballon.css({top:dY})
            rebondirGauche();
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
    
    function rebondirDroite(dt){
        animId = requestAnimationFrame(rebondirDroite);
        if (ballon.position().top<=dY && ballon.position().left < 735 + ballon.width() ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            var posX = vH*t + dX;
            ballon.css({left:posX,top:posY});
            imgBallon.css({rotate:"+=1deg"})
            t+=1/31;
        } else if (ballon.position().left < 735+ballon.width() ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            vH *= 3/4;
            dX = ballon.position().left;
            ballon.css({top:dY})
            rebondirDroite();
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
    
    function rebondir(dt){
        animId = requestAnimationFrame(rebondir);

        if (ballon.position().top <= dY ) {
            var posY =  (g/2)*Math.pow(t,2) - vV*t + dY ;
            ballon.css({top:posY});
            t+=1/31;
        } else if ( vV > 150 ) {
            cancelAnimationFrame(animId);
            //animId = requestAnimationFrame(rebondirGauche);
            t = 0;
            vV *=3/4;
            dX = ballon.position().left;
            ballon.css({top:dY})
            rebondir();
            
        } else {
            cancelAnimationFrame(animId);
            console.log("terminé");
            exo.poursuivreExercice(1000,3000);
        }
    }
}

// Evaluation : doit toujours retourner "juste" "faux" ou "rien"
exo.evaluer = function() {
   return resEval;
}

// Correction (peut rester vide)
exo.corriger = function() {
    console.log("corriger",aSoluce);
    for ( var i=0; i< 3 ; i++ ) {
        //aPanneau[aSoluce[i]].css({opacity:0.3});
        aPanneau[i].css({opacity:0.3});
        //aFilet[aSoluce[i]].css({opacity:0.3});
        aFilet[i].css({opacity:0.3});
    }
    aPanneau[aSoluce[exo.indiceQuestion]].css({opacity:1});
    aFilet[aSoluce[exo.indiceQuestion]].css({opacity:1});
}

// Création des contrôles permettant au prof de paraméter l'exo
exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:2,
        nom:"totalQuestion",
        texte:exo.txt.opt1
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.opt2,
        aLabel:exo.txt.label2,
        aValeur:[1,2]
    });
    exo.blocParametre.append(controle);
    //
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:24,
        taille:3,
        nom:"tempsExo",
        texte:exo.txt.opt3
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))