# $id$
# chemin.tcl configuration file

#*************************************************************************
#  Copyright (C) 2002 André Connes <andre.connes@toulouse.iufm.fr>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#**************************************************************************
#  File  : chemin.tcl
#  Author  : André Connes <andre.connes@toulouse.iufm.fr>
#  Modifier:
#  Date    : 19/05/2004
#  Licence : GNU/GPL Version 2 ou plus
#
#  Description:
#  ------------
#
#  @version    $Id: chemin.conf,v 1.32 2006/03/27 13:13:15 abuledu_andre Exp $
#  @author     André Connes
#  @modifier
#  @project    Le terrier
#  @copyright  André Connes
#
#***********************************************************************

set glob(version) "7.1"

set glob(bgcolor) #ffff80 ;# couleur du fond
set glob(width) 600 ;#800
set glob(height) 400 ;#600
set glob(org) 1 ;#translation de l'origine des coordonnées pour ajustement
set glob(attente) 5  ;#temps d'attente entre deux sessions en seconde(s)

set glob(langue) "fr" ;# français par défaut mais voir lang.conf

set glob(nbrow) 10
set glob(nbcol) 15
set glob(wlen)  40 ;# largeur d'une case de la grille
set glob(hlen)  40 ;# hauteur d'une case de la grille
set glob(boucle) 0    ;# nb de parcours traités
set glob(bouclemax) 5 ;# nb max de parcours à traiter (sentinelle)
set glob(nbepassable) 2 ;# nb max d'erreurs permettant l'appreciation : passable

# ###################################################
# en général, ne pas modifier ci-dessous svp        #
# Cependant voir en fin de fichier le code pour     #
#     les répertoires des exercices (si nécessaire) #
# ###################################################

# couleurs des cases
set glob(celDaltonien)	#00ffc0	;# case non activée
set glob(donDaltonien)	#cc00ff	;# case activée
set glob(errDaltonien)	#ff0000	;# erreur
set glob(celNonDaltonien) #00ffa0
set glob(donNonDaltonien) #1de05a
set glob(errNonDaltonien) #ff0000

# gestion des répertoires Commun/Individuel,de la langue, des traces-élèves suivant l'OS
#   user=répertoire+identifiant-élève
if { $tcl_platform(platform) == "unix" } {
  set glob(platform) unix
  set glob(home_chemin) [file join $env(HOME) leterrier chemin]
  set glob(trace_dir) [file join $glob(home_chemin) log] ;# on attend mieux !
  set glob(trace_user) $glob(trace_dir)/$tcl_platform(user)
  set glob(home_reglages) [file join $glob(home_chemin) reglages]
  set glob(home_msgs) [file join $glob(home_chemin) msgs]
  set glob(progaide) runbrowser
  set glob(wish) wish

  #si il n'y a pas de répertoire Img1.3 ou Img1.2, on essaye de charger les
  #libs systèmes ...
  package require Img
} elseif { $tcl_platform(platform) == "windows" } {
  set glob(platform) windows
  set glob(home_chemin) [file dir $argv0] ;# cad .
  set glob(trace_dir) [file join $glob(home_chemin) chemin log]
  set glob(trace_user) "$glob(trace_dir)/eleve" ;#forcer le nom (cf chemin.tcl)
  set a ""
  catch {set a $env(USER) }
  if { $a != "" } {
  # répertoire pour tous les utilisateurs
  #     catch { set glob(home_chemin) [file join $env(ALLUSERSPROFILE) leterrier chemin] }
  # ou répertoire pour l'utilisateur loggé
        catch { set glob(home_chemin) [file join $env(USERPROFILE) leterrier chemin] }
        set glob(trace_dir) [file join $glob(home_chemin) log]
        set glob(trace_user) $glob(trace_dir)/$env(USER)
        }
  set glob(home_msgs) [file join $glob(home_chemin) msgs]
  set glob(home_reglages) [file join $glob(home_chemin) reglages]
  set glob(progaide) shellexec.exe
  set glob(wish) wish
  package require Img
} else {
  set glob(platform) undef
  set glob(home_chemin) undef
  set glob(trace_dir) undef
  set glob(trace_user) undef
  set glob(home_reglages) undef
  set glob(home_msgs) undef
  set glob(progaide) undef
  set glob(wish) undef
}

#
# code à adapter pour gestion des répertoires des exercices
#

set fname [file join $glob(home_reglages) dir_exos.conf]
if {[file exists $fname]} {
        set f [open $fname "r"]
        set glob(dir_exos) [gets $f]
        close $f
}
