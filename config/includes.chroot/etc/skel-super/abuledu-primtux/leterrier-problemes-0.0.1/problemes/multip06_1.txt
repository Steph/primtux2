set etapes 1
set niveaux {0 1 2 3 4 5}
::1
set niveau 1
set ope {{2 5}}
set interope {{1 8 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set volatil 0
set operations [list [list [expr $ope1 ]+[expr $ope1]+[expr $ope1 ]] [list [expr $ope1]*3] [list 3*[expr $ope1]]]
set enonce "L'album.\nJe colle mes photos dans un album.\nJ'ai rempli 5 pages.\nSur chaque page, j'ai mis $ope1 photos.\nCombien ai-je coll� de photos?"
set cible {{3 2 {} source0} {3 2 {} source0} {3 2 {} source0} {3 2 {} source0} {3 2 {} source0}}
set intervalcible 20
set taillerect 45
set orgy 40
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 670
set source {image.gif}
set orient 0
set labelcible {{Page 1} {Page 2} {Page 3} {Page 4} {Page 5}}
set quadri 0
set reponse [list [list {1} [list {J'ai coll�} [expr $ope1*5] {photos en tout.}]]]
set ensembles [list [expr $ope1] [expr $ope1] [expr $ope1] [expr $ope1] [expr $ope1]]
set canvash 300
set c1height 160
set opnonautorise {0}
::