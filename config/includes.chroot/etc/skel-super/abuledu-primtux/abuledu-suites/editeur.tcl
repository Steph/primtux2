############################################################################
# Copyright (C) 2003 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : editeur.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: editeur.tcl,v 1.2 2005/12/31 13:24:14 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#Editeur.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

##################################"sourcing
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source eval.tcl
source msg.tcl

inithome
initlog $plateforme $ident
changehome

set fichier [file join $basedir aide index.htm]
bind . <F1> "exec $progaide \042$fichier\042 &"

global arg
set arg [lindex $argv 0]

. configure -background white
frame .geneframe -background white
pack .geneframe -side left

#variables
#################################################génération interface
proc interface { what} {
global sysFont arg activelist indexpage listdata
variable titre
variable type
variable nbcol
variable nbrow
variable reverse
variable raison
variable depart
variable trace
variable trous
variable nombrexact
variable pasmax
variable indmax
variable maxparcours
variable aplusb
variable amoinsb
variable aplusbplusc
variable freqlaby

catch {destroy .geneframe}

catch {destroy .leftframe}

frame .leftframe -background cyan 
pack .leftframe -side left -fill y

frame .geneframe -background white
pack .geneframe -side left -anchor n
#label .leftframe.lignevert1 -bg red -text "" -font {Arial 1} -height 40
#pack .leftframe.lignevert1 -side right -expand 1 -fill y 
######################leftframe.frame1
frame .leftframe.frame1 -background cyan 
pack .leftframe.frame1 -side top

label .leftframe.frame1.lab1 -foreground red -bg cyan -text [mc $arg] -font $sysFont(l)
pack .leftframe.frame1.lab1 -side top -fill x -pady 5

label .leftframe.frame1.lab2 -bg blue -foreground white -text [mc {Scenario}] -font $sysFont(l)
pack .leftframe.frame1.lab2 -side top -fill x -expand 1 -pady 5

listbox .leftframe.frame1.listsce -yscrollcommand ".leftframe.frame1.scrollpage set" -width 15 -height 12
scrollbar .leftframe.frame1.scrollpage -command ".leftframe.frame1.listsce yview" -width 7
pack .leftframe.frame1.listsce .leftframe.frame1.scrollpage -side left -fill y -expand 1 -pady 10
bind .leftframe.frame1.listsce <ButtonRelease-1> "changelistsce %x %y"

.leftframe.frame1.listsce delete 0 end
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	.leftframe.frame1.listsce insert end [lindex [lindex [lindex $listdata $k] 0] 0]
	}
.leftframe.frame1.listsce selection set $indexpage

############################leftframe.frame2
frame .leftframe.frame2 -background cyan
pack .leftframe.frame2 -side top 
button .leftframe.frame2.but0 -text [mc {Nouveau}] -command "additem" -activebackground white
pack .leftframe.frame2.but0 -pady 5 -side top
button .leftframe.frame2.but1 -text [mc {Supprimer}] -command "delitem" -activebackground white
pack .leftframe.frame2.but1 -pady 5 -side top


########################leftframe.frame3
frame .leftframe.frame3 -background cyan
pack .leftframe.frame3 -side top


button .leftframe.frame3.but0 -text [mc {ok}] -command "fin" -activebackground white
pack .leftframe.frame3.but0 -side bottom



######################geneframe
set indrow 0
label .geneframe.labtitre -bg white -text $titre -font $sysFont(l)
grid .geneframe.labtitre -row $indrow -column 0 -columnspan 5 -sticky news
incr indrow

radiobutton .geneframe.sel1 -text [mc {Suite arithmetique}] -variable type -relief flat -value 0 -background white -activebackground white -command "enregistre_sce; charge $indexpage"
radiobutton .geneframe.sel2 -text [mc {Suite geometrique}] -variable type -relief flat -value 1 -background white -activebackground white -command "enregistre_sce; charge $indexpage"
radiobutton .geneframe.sel11 -text [mc {Suite aleatoire}] -variable type -relief flat -value 2 -background white -activebackground white -command "enregistre_sce; charge $indexpage"
radiobutton .geneframe.sel12 -text [mc {Laby nombre}] -variable type -relief flat -value 3 -background white -activebackground white -command "enregistre_sce; charge $indexpage"  
radiobutton .geneframe.sel13 -text [mc {Laby decomposition}] -variable type -relief flat -value 4 -background white -activebackground white -command "enregistre_sce; charge $indexpage"  
grid .geneframe.sel1 -row $indrow -column 0 -sticky news
grid .geneframe.sel2 -row $indrow -column 1 -sticky news
grid .geneframe.sel11 -row $indrow -column 2 -sticky news
grid .geneframe.sel12 -row $indrow -column 3 -sticky news
grid .geneframe.sel13 -row $indrow -column 4 -sticky news
incr indrow

label .geneframe.labrow -bg white -text [mc {Nombre de lignes :}] 
entry .geneframe.entrow -bg white -width 3
grid .geneframe.labrow -row $indrow -column 0 -sticky news
grid .geneframe.entrow -row $indrow -column 1 -sticky w

label .geneframe.labcol -bg white -text [mc {Nombre de colonnes :}]
entry .geneframe.entcol -bg white -width 3
grid .geneframe.labcol -row $indrow -column 2 -sticky news
grid .geneframe.entcol -row $indrow -column 3 -sticky w
.geneframe.entcol insert end $nbcol
.geneframe.entrow insert end $nbrow
incr indrow

radiobutton .geneframe.sel3 -text [mc {Parcours a l'endroit}] -variable reverse -relief flat -value 0 -background white -activebackground white 
radiobutton .geneframe.sel4 -text [mc {Parcours a l'envers}] -variable reverse -relief flat -value 1 -background white -activebackground white 
grid .geneframe.sel3 -row $indrow -column 0 -sticky news
grid .geneframe.sel4 -row $indrow -column 2 -sticky news

if {$type != 4} {
checkbutton .geneframe.chk20 -text [mc {Generer le chemin le plus long}] -variable maxparcours -relief flat -bg white -activebackground white
grid .geneframe.chk20 -row $indrow -column 3 -columnspan 2 -sticky e
}
incr indrow
label .geneframe.raison -bg white -text [mc {Raison (pas) de la suite :}]
entry .geneframe.entraison -bg white -width 4
grid .geneframe.raison -row $indrow -column 0 -columnspan 2 -sticky w
grid .geneframe.entraison -row $indrow -column 2 -sticky w
.geneframe.entraison insert end $raison
if {$type == 4} {.geneframe.raison configure -text [mc {Nombre cible pour la decomposition :}]}

if {$type == 2} {
label .geneframe.pasmax -bg white -text [mc {Raison max :}]
entry .geneframe.entpasmax -bg white -width 4
grid .geneframe.pasmax -row $indrow -column 3 -sticky w
grid .geneframe.entpasmax -row $indrow -column 4 -sticky w
.geneframe.entpasmax insert end $pasmax
}
incr indrow

set indcol 0
if {$type != 4} {
label .geneframe.depart -bg white -text [mc {Nombre de depart :}]
entry .geneframe.entdepart -bg white -width 4
grid .geneframe.depart -row $indrow -column 0 -columnspan 2 -sticky w
grid .geneframe.entdepart -row $indrow -column 2 -sticky w
.geneframe.entdepart insert end $depart
set indcol 3
}

if {$type == 2} {
label .geneframe.indmax -bg white -text [mc {Nombre de depart max :}]
entry .geneframe.entindmax -bg white -width 4
grid .geneframe.indmax -row $indrow -column $indcol -sticky w
grid .geneframe.entindmax -row $indrow -column [expr $indcol +1] -sticky w
.geneframe.entindmax insert end $indmax
}
if {$type != 4 || $type == 2} {incr indrow}

if {$type == 4 || $type == 3} {
label .geneframe.ope -bg white -text [mc {Types d'ecritures numeriques :}]
checkbutton .geneframe.chk40 -text [mc {a + b}] -variable aplusb -relief flat -bg white -activebackground white
checkbutton .geneframe.chk41 -text [mc {a - b}] -variable amoinsb -relief flat -bg white -activebackground white
checkbutton .geneframe.chk42 -text [mc {a + b + c}] -variable aplusbplusc -relief flat -bg white -activebackground white
grid .geneframe.ope -row $indrow -column 0 -sticky w
grid .geneframe.chk40 -row $indrow -column 1 -sticky w
grid .geneframe.chk41 -row $indrow -column 2 -sticky w
grid .geneframe.chk42 -row $indrow -column 3 -sticky w
incr indrow
}

if {$type == 3} {
label .geneframe.flaby -bg white -text [mc {Frequence des substitutions :}]
radiobutton .geneframe.sel71 -text [mc {peu nombreux}] -variable freqlaby -relief flat -value 0 -background white -activebackground white 
radiobutton .geneframe.sel81 -text [mc {nombreux}] -variable freqlaby -relief flat -value 1 -background white -activebackground white 
radiobutton .geneframe.sel91 -text [mc {tous}] -variable freqlaby -relief flat -value 2 -background white -activebackground white
grid .geneframe.flaby -row $indrow -column 0 -sticky w 
grid .geneframe.sel71 -row $indrow -column 1 -sticky w
grid .geneframe.sel81 -row $indrow -column 2 -sticky w
grid .geneframe.sel91 -row $indrow -column 3 -sticky w
incr indrow
}


radiobutton .geneframe.sel5 -text [mc {Les cases parcourues sont marquees}] -variable trace -relief flat -value 1 -background white -activebackground white 
radiobutton .geneframe.sel6 -text [mc {Les cases parcourues ne sont pas marquees}] -variable trace -relief flat -value 0 -background white -activebackground white 
grid .geneframe.sel5 -row $indrow -column 0 -columnspan 2 -sticky w
grid .geneframe.sel6 -row $indrow -column 2 -columnspan 2 -sticky w
incr indrow

if {$type != 4} {
checkbutton .geneframe.chk0 -text [mc {Nombres caches :}] -variable trous -relief flat -bg white -activebackground white -command "montreparam $indrow"
grid .geneframe.chk0 -row $indrow -column 0 -sticky w
incr indrow
if {$trous == 1} {montreparam $indrow}
}
}

proc montreparam {indrow} {
variable trous
variable type
catch {
destroy .geneframe.chk10 .geneframe.sel7 .geneframe.sel8 .geneframe.sel9
} 
if {$type == 2 && $trous ==1} {
checkbutton .geneframe.chk10 -text [mc {Retrouver le nombre exact}] -variable nombrexact -relief flat -bg white -activebackground white
grid .geneframe.chk10 -row $indrow -column 2 -columnspan 2 -sticky w
}
incr indrow

if {$trous ==1} {
radiobutton .geneframe.sel7 -text [mc {peu nombreux}] -variable freq -relief flat -value 0 -background white -activebackground white 
radiobutton .geneframe.sel8 -text [mc {nombreux}] -variable freq -relief flat -value 1 -background white -activebackground white 
radiobutton .geneframe.sel9 -text [mc {tous}] -variable freq -relief flat -value 2 -background white -activebackground white 
grid .geneframe.sel7 -row $indrow -column 0 -sticky w
grid .geneframe.sel8 -row $indrow -column 1 -sticky w
grid .geneframe.sel9 -row $indrow -column 2 -sticky w
}
}
####################################################################"""
proc charge {index} {
global listdata activelist indexpage totalpage arg  Home Home_data
variable titre
variable type
variable nbcol
variable nbrow
variable reverse
variable raison
variable depart
variable trace
variable trous
variable freq
variable nombrexact
variable pasmax
variable indmax
variable maxparcours
variable aplusb
variable amoinsb
variable aplusbplusc
variable freqlaby

set ext .conf

set f [open [file join $Home_data $arg$ext] "r"]
set listdata [gets $f]
close $f

set totalpage [llength $listdata] 
set indexpage $index
set activelist [lindex $listdata $indexpage]
set titre [lindex $activelist 0]
set type [lindex [lindex $activelist 1] 0]
set nbrow [lindex [lindex $activelist 1] 1]
set nbcol [lindex [lindex $activelist 1] 2]
set reverse [lindex [lindex $activelist 1] 3]
set raison [lindex [lindex $activelist 1] 4]
set depart [lindex [lindex $activelist 1] 5]
set trace [lindex [lindex $activelist 1] 6]
set trous [lindex [lindex $activelist 1] 7]
set freq [lindex [lindex $activelist 1] 8]
set nombrexact [lindex [lindex $activelist 1] 9]
set pasmax [lindex [lindex $activelist 1] 10]
set indmax [lindex [lindex $activelist 1] 11]
set maxparcours [lindex [lindex $activelist 1] 12]
set freqlaby [lindex [lindex $activelist 1] 13]
set aplusb [lindex [lindex $activelist 1] 14]
set amoinsb [lindex [lindex $activelist 1] 15]
set aplusbplusc [lindex [lindex $activelist 1] 16]
interface $arg
}



###################################################################################"
proc enregistre_sce {} {
global indexpage listdata activelist totalpage arg Home Home_data
set ext .conf
variable titre
variable type
variable nbcol
variable nbrow
variable reverse
variable raison
variable depart
variable trace
variable trous
variable freq
variable nombrexact
variable pasmax
variable indmax
variable maxparcours
variable aplusb
variable amoinsb
variable aplusbplusc
variable freqlaby

catch {
set nbrow [expr int([.geneframe.entrow get])]
if {$nbrow <1} {set nbrow 1}
if {$nbrow > 16} {set nbrow 16}
}

catch {
set nbcol [expr int([.geneframe.entcol get])]
if {$nbcol <2} {set nbcol 2}
if {$nbcol > 24} {set nbcol 24}
}

catch {
set raison [expr [.geneframe.entraison get]]
if {$raison <0} {set raison 0}
}

catch {
set pasmax [expr [.geneframe.entpasmax get]]
if {$pasmax < $raison} {set pasmax $raison}
}

catch {
set depart [expr [.geneframe.entdepart get]]
if {$depart <0} {set depart 0}
if {$type == 1} {
if {$depart ==0} { set depart 1}
}
}

catch {
set indmax [expr [.geneframe.entindmax get]]
if {$indmax < $depart} {set indmax $depart}
}


set activelist [.leftframe.frame1.listsce get $indexpage]\040\173\040$type\040$nbrow\040$nbcol\040$reverse\040$raison\040$depart\040$trace\040$trous\040$freq\040$nombrexact\040$pasmax\040$indmax\040$maxparcours\040$freqlaby\040$aplusb\040$amoinsb\040$aplusbplusc\175

set listdata [lreplace $listdata $indexpage $indexpage $activelist]
set f [open [file join $Home_data $arg$ext] "w"]
puts $f $listdata
close $f
}


###################################################################
proc changescalemin {min} {
if {[.geneframe.scale$min get] > [.geneframe.scale$min$min get] } { 
.geneframe.scale$min$min set [.geneframe.scale$min get] 
}
}

proc changescalemax {max} {
if {[.geneframe.scale$max$max get] < [.geneframe.scale$max get] } { 
.geneframe.scale$max set [.geneframe.scale$max$max get] 
}
}


proc changelistsce {x y} {
global listdata
enregistre_sce
set ind [.leftframe.frame1.listsce index @$x,$y]
charge $ind
}

proc fin {} {
enregistre_sce
destroy .
}

#######################################################################
proc delitem {} {
global listdata indexpage totalpage arg Home Home_data
set ext .conf
	if {$totalpage < 2} {
	tk_messageBox -message [mc {Impossible de supprimer la fiche}] -type ok -title [mc $arg]
	return
	}
set response [tk_messageBox -message [format [mc {Voulez-vous vraiment supprimer la fiche %1$s ?}] [lindex [lindex [lindex $listdata $indexpage] 0] 0]] -type yesno -title [mc $arg]]
	if {$response == "yes"} {
	set totalpage [expr $totalpage - 1]
		set listdata [lreplace $listdata $indexpage $indexpage]
     		if {$indexpage > 0} {
     		set indexpage [expr $indexpage - 1]						
		} 
	set f [open [file join $Home_data $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage

	} 

}


proc additem {} {
global listdata indexpage totalpage 
enregistre_sce
catch {destroy .nomobj}
toplevel .nomobj -background grey -width 250 -height 100
wm geometry .nomobj +50+50
frame .nomobj.frame -background grey -width 250 -height 100
pack .nomobj.frame -side top
label .nomobj.frame.labobj -font {Helvetica 10} -text [mc {Nom du scenario :}] -background grey
pack .nomobj.frame.labobj -side top 
entry .nomobj.frame.entobj -font {Helvetica 10} -width 10
pack .nomobj.frame.entobj -side top 
button .nomobj.frame.ok -background gray75 -text [mc {Ok}] -command "verifnomobj"
pack .nomobj.frame.ok -side top -pady 10
}

proc verifnomobj {} {
global nom listdata indexpage totalpage arg Home Home_data

set ext .conf
set nom [join [.nomobj.frame.entobj get] ""]
	if {$nom !=""} {
	set indexpage $totalpage
	incr totalpage
	lappend listdata $nom\040\040\173\0400\0404\0404\0400\0401\0401\0401\0400\0400\0400\0401\0401\0400\0400\0400\0400\0400\175
	set f [open [file join $Home_data $arg$ext] "w"]
	puts $f $listdata
	close $f
	charge $indexpage
	}
catch {destroy .nomobj}
}


charge 0
