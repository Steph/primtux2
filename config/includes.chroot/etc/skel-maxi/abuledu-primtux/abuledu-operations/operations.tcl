############################################################################
# Copyright (C) 2002 Eric Seigne
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : operations.tcl
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 01/11/2004
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version    $Id: operations.tcl,v 1.3 2004/12/20 12:07:39 david Exp $
# @author     David Lucardi
# @project
# @copyright  Eric Seigne
#
#
#########################################################################
#!/bin/sh
#operations.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}

set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)

source fonts.tcl
source path.tcl
source msg.tcl



init $plateforme
inithome
initlog $plateforme $ident
changehome


proc majmenu {} {
	global Home
	variable soustret
	catch {.menu.action.m1 delete 0 end}
	catch {.menu.action.m2 delete 0 end}
	catch {.menu.action.m3 delete 0 end}
	catch {.menu.action.m4 delete 0 end}
	catch {.menu.action.m5 delete 0 end}
	catch {.menu.action.m6 delete 0 end}
	catch {.menu.action.m7 delete 0 end}
	if {$soustret==0} {
		set soust soustractions2.tcl
	} else {
		set soust soustractions.tcl
	}
	set f [open [file join $Home reglages additions.conf] "r"]
	set listdata [gets $f]
	close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	   	set nline [lrange [lindex $listdata $k] 0 0]
	   	set nline_size [string length $nline]
			set scen [string range [lrange [lindex $listdata $k] 0 0] 1 [expr $nline_size - 2]]
	   	.menu.action.m1 add command -label "$scen" -command "lanceappli additions.tcl $k"
	}
	set f [open [file join $Home reglages soustractions.conf] "r"]
	set listdata [gets $f]
	close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	   	set nline [lrange [lindex $listdata $k] 0 0]
	  		set nline_size [string length $nline]
	  		set scen [string range [lrange [lindex $listdata $k] 0 0] 1 [expr $nline_size - 2]]
	   	.menu.action.m2 add command -label "$scen" -command "lanceappli $soust $k"
	}

	set f [open [file join $Home reglages multiplications.conf] "r"]
	set listdata [gets $f]
	close $f
	for {set k 0} {$k < [llength $listdata]} {incr k 1} {
	   	set nline [lrange [lindex $listdata $k] 0 0]
	   	set nline_size [string length $nline]
			set scen [string range [lrange [lindex $listdata $k] 0 0] 1 [expr $nline_size - 2]]
	   	.menu.action.m3 add command -label "$scen" -command "lanceappli multiplications.tcl $k"
	}
}



proc interface {} {
  global iwish basedir Home plateforme progaide sysFont abuledu prof
  variable soustret
  
  set f [open [file join $Home reglages repert.conf] "r"]
  set listdata [gets $f]
  close $f
  set repert $listdata
  
  
  set f [open [file join $Home reglages regsoust.conf] "r"]
  set listdata [gets $f]
  close $f
  set soustret $listdata
  
  if {$soustret==0} {
    set soust soustractions2.tcl
  } else {
    set soust soustractions.tcl
  }
  
  
  . configure -background #ffff80
  catch {
    destroy .menu
    destroy .wleft
    destroy .wcenter
    destroy .wright
  }
  menu .menu
  
  
  
  # Creation du menu Fichier
  # ------------------------------
  menu .menu.fichier -tearoff 0
  .menu add cascade -label [mc "Fichier"] -menu .menu.fichier
  if {($abuledu == 0) || ($repert==0) || ($prof==1)} {
    .menu.fichier add cascade -label [mc "Editeur"] -menu .menu.fichier.editeur
  } else {
    .menu.fichier add cascade -label [mc "Editeur"] -menu .menu.fichier.editeur -state disabled
  }
  menu .menu.fichier.editeur -tearoff 0
  
  
  
  # Sub-Menu Fichier - Editeur
  .menu.fichier.editeur add command -label [mc "Additions"] -command "exec $iwish edit.tcl additions &"
  .menu.fichier.editeur add command -label [mc "Soustractions"] -command "exec $iwish edit.tcl soustractions &"
  .menu.fichier.editeur add command -label [mc "Multiplications"] -command "exec $iwish edit.tcl multiplications &"

  
  
  .menu.fichier add command -label [mc "Bilans"] -command "exec $iwish bilan.tcl &"
  .menu.fichier add sep
  .menu.fichier add command -label [mc "Quitter"] -command exit
  
  
  # Creation du menu Activites
  # ------------------------------
  menu .menu.action -tearoff 0
  .menu add cascade -label [mc "Activit�s"] -menu .menu.action
  menu .menu.action.m1 -tearoff 0
  .menu.action add cascade -label [mc "Additions"] -menu .menu.action.m1
  menu .menu.action.m2 -tearoff 0 
  .menu.action add cascade -label [mc "Soustractions"] -menu .menu.action.m2 
  menu .menu.action.m3 -tearoff 0 
  .menu.action add cascade -label [mc "Multiplications"] -menu .menu.action.m3 
  menu .menu.action.m4 -tearoff 0 
 
  
  
  # Creation du menu Reglages
  # ------------------------------
  menu .menu.options -tearoff 0
  .menu add cascade -label [mc "R�glages"] -menu .menu.options
  

  menu .menu.options.regsoust -tearoff 0 
  set message ""
  append message [mc "Soustraction"]
  .menu.options add cascade -label $message -menu .menu.options.regsoust
  set message ""
  append message [mc "Retenue classique"]
  .menu.options.regsoust add radio -label $message -variable soustret -value 1 -command "setsoustret 1"
  set message ""
  append message [mc "Retenue anglo-saxonne"]
  .menu.options.regsoust add radio -label $message -variable soustret -value 0 -command "setsoustret 0"
  

  set ext .msg
  menu .menu.options.lang -tearoff 0 
  set message ""
  append message [mc "Langue"]
  .menu.options add cascade -label $message -menu .menu.options.lang
  
  foreach i [glob [file join  $basedir msgs *$ext]] {
    set langue [string map {.msg ""} [file tail $i]]
    .menu.options.lang add radio -label $langue -variable langue -command "setlang $langue"
  }
  
  
  menu .menu.options.dossier -tearoff 0 
  .menu.options add cascade -label [mc "Dossier de travail"] -menu .menu.options.dossier
  .menu.options.dossier add radio -label [mc "Individuel"] -variable repert -value 0 -command "changehomeupdate"
  .menu.options.dossier add radio -label [mc "Commun"] -variable repert -value 1 -command "changehomeupdate"
  
  
  changehome
  
  if {$plateforme == "windows"} {
    .menu add command -label [mc "Utilisateur"] -command "setwindowsusername"
  }
  
  menu .menu.aide -tearoff 0
  .menu add cascade -label " ? " -menu .menu.aide
  set fichier [file join [pwd] aide index.htm]
  .menu.aide add command -label [mc "Aide"] -command "exec $progaide file:$fichier &"
  .menu.aide add command -label [mc "� propos ..."] -command "source apropos.tcl"
  
  . configure -menu .menu
  
  . configure -background blue

  frame .wleft -background blue -height 540 -width 100
  pack .wleft -side left
  
  
  # NSE 20090818 - CREATION d'un BOUTTON avec ACTION li�e
  # *****************************************************
  button .wleft.labA1 -command "wcenter_clear;show additions" -image [image create photo -file [file join sysdata add_100.gif]] -text [mc "Additions"] -compound top -width 100 -height 110
  pack .wleft.labA1 -pady 10 -padx 10
  
  button .wleft.labA2 -command "wcenter_clear;show soustractions" -image [image create photo -file [file join sysdata sous_100.gif]] -text [mc "Soustractions"] -compound top -width 100 -height 110
  pack .wleft.labA2 -pady 10
  
  button .wleft.labA4 -command "wcenter_clear;show soustractions2" -image [image create photo -file [file join sysdata sous2_100.gif]] -text [mc "Soustractions Anglo"] -compound top -width 100 -height 110
  pack .wleft.labA4 -pady 10
  
  button .wleft.labA3 -command "wcenter_clear;show multiplications" -image [image create photo -file [file join sysdata mult_100.gif]] -text [mc "Multiplications"] -compound top -width 100 -height 110
  pack .wleft.labA3 -pady 10
  

    
  frame .wcenter -background blue -height 540 -width 400
  pack .wcenter -side left

  frame .wcenter.p1a -background blue -height 10 -width 400
  pack .wcenter.p1a -side top

  frame .wcenter.p2 -background blue -height 120 -width 400
  pack .wcenter.p2 -side top
  frame .wcenter.p2a -background blue -height 30 -width 400
  pack .wcenter.p2a -side top

  frame .wcenter.p3 -background blue -height 120 -width 400
  pack .wcenter.p3 -side top
  frame .wcenter.p3a -background blue -height 30 -width 400
  pack .wcenter.p3a -side top

  frame .wcenter.p4 -background blue -height 120 -width 400
  pack .wcenter.p4 -side top
  frame .wcenter.p4a -background blue -height 30 -width 400
  pack .wcenter.p4a -side top

  frame .wcenter.p5 -background blue -height 110 -width 400
  pack .wcenter.p5 -side top 
  frame .wcenter.p5a -background blue -height 10 -width 400
  pack .wcenter.p5a -side top



  frame .wright -background blue -height 530 -width 200
  pack .wright -side left

  frame .wright.p1 -background blue -height 100 -width 200
  pack .wright.p1 -side top

  frame .wright.p2 -background blue -height 300 -width 200
  pack .wright.p2 -side top

  frame .wright.p3 -background blue -height 100 -width 200
  pack .wright.p3 -side bottom

  
  set myimage1 [image create photo -file sysdata/background_new.gif]
  label .wright.p1.imagedisplayer2 -image $myimage1 -background blue
  pack .wright.p1.imagedisplayer2 -side top


  button .wright.p3.but_clear -command "wcenter_clear" -image [image create photo -file [file join sysdata fgauche.gif]]
  pack .wright.p3.but_clear -side left

  button .wright.p3.but_quitter -command exit -image [image create photo -file [file join sysdata quitter_minus.gif]]
  pack .wright.p3.but_quitter -side left

  
  majmenu

}


proc bientot {} {
  set answer [tk_messageBox -message [mc "C'est pour bient�t !"] -type ok]
}
interface

############
# Bindings #
############
bind . <Control-q> {exit}

bind . <FocusIn> {majmenu}



proc lanceappli {appli niveau} {
  global iwish
  set appli [file join $appli]
  #appli.tcl index_dans_le_sc�nario
  exec $iwish $appli $niveau &
}



proc changehomeupdate {} {
  changehome
  interface
  majmenu
}

proc setsoustret {what} {
  global baseHome
  set f [open [file join $baseHome reglages regsoust.conf] "w"]
  puts $f $what
  close $f
  interface
}


proc setlang {lang} {
	  global env plateforme baseHome
	  set env(LANG) $lang
	  set f [open [file join $baseHome reglages lang.conf] "w"]
	  puts $f $lang
	  close $f

	  ::msgcat::mclocale $lang
	  ::msgcat::mcload [file join [file dirname [info script]] msgs]
	  interface
}


proc wcenter_clear {} {
  	global Home


	  	catch {destroy .wcenter.p1a}
		catch {destroy .wcenter.p2}
		catch {destroy .wcenter.p2a}
		catch {destroy .wcenter.p3}
		catch {destroy .wcenter.p3a}
		catch {destroy .wcenter.p4}
		catch {destroy .wcenter.p4a}
		catch {destroy .wcenter.p5}
		catch {destroy .wcenter.p5a}

	  frame .wcenter.p1a -background blue -height 10 -width 400
	  pack .wcenter.p1a -side top
	  frame .wcenter.p2 -background blue -height 120 -width 400
	  pack .wcenter.p2 -side top
	  frame .wcenter.p2a -background blue -height 30 -width 400
	  pack .wcenter.p2a -side top
	  frame .wcenter.p3 -background blue -height 120 -width 400
	  pack .wcenter.p3 -side top
	  frame .wcenter.p3a -background blue -height 30 -width 400
	  pack .wcenter.p3a -side top
	  frame .wcenter.p4 -background blue -height 120 -width 400
	  pack .wcenter.p4 -side top
	  frame .wcenter.p4a -background blue -height 30 -width 400
	  pack .wcenter.p4a -side top
	  frame .wcenter.p5 -background blue -height 110 -width 400
	  pack .wcenter.p5 -side top 
	  frame .wcenter.p5a -background blue -height 10 -width 400
	  pack .wcenter.p5a -side top
}


proc show {what} {
  global Home sysFont
  
  set ind 0
  set ext .conf
  set ext2 1.gif

  pack .wcenter -padx 15

    if {$what == "additions"} {
      set fic $what$ext
      set swhat "add"
      set part "p2"
    }
    if {$what == "soustractions"} {
      set fic $what$ext
      set swhat "sous"
      set part "p3"
    }
    if {$what == "soustractions2"} {
      set fic soustractions$ext
      set swhat "sous2"
      set part "p4"
    }
    if {$what == "multiplications"} {
      set fic $what$ext
      set swhat "mult"
      set part "p5"
    }

    set f [open [file join  $Home reglages $fic] "r"]

    set listdata [gets $f]
    close $f
    set nb_total [llength $listdata]
    set nb 5
    set c "_"
    # Creation d une boutton avec un chiffre pour les 5 premiers scenarios
    #   dans la zone .wcenter.p1.bt1
    for {set k 0} {$k <= 4} {incr k 1} {
  	   set nline [lrange [lindex $listdata $k] 0 0]
  	   set nline_size [string length $nline]
  	   
  	   set titre [string range [lrange [lindex $listdata $k] 0 0] 0 [expr $nline_size - 1]]
  	   set indaff [expr $ind + 1]
  	   set k1 [expr $k + 1]
  	   button .wcenter.$part.bt$ind -bg blue -text "$ind - $titre" -width 60 -font $sysFont(ms) -command "lanceappli $what.tcl $k" -image [image create photo -file [file join sysdata lapin$k1.gif]]
       grid .wcenter.$part.bt$ind -row [expr int($ind/8)] -column [expr int(fmod($ind,8))]
       
       	   incr ind
     } 

}







proc showparcours {} {
  global Home
  catch {destroy .framebottom}
  frame .framebottom -width 400 -bg grey
  pack .framebottom -side top -fill both -expand yes
  set ind 0
  set what parcours
  set ext .par
  foreach i [glob [file join $Home operations  $what*$ext]] {
    set f [open [file join  $Home operations [file tail $i]] "r"]
    set parcours [gets $f]
    close $f

    set tmp [lindex [lindex $parcours 0] 0]
    regsub -all {.txt} $tmp "" tmp
    set tit [file tail $i]
    regsub -all {.txt} $tit "" tit

    set indp [string range $tmp end end]
    
    button .framebottom.enonce$ind -bg grey -text "$ind - $tit" -width 50 -command ".framebottom.enonce$ind configure -fg blue; catch {execp operations$indp.tcl [lindex [lindex $parcours 0] 0] [lindex [lindex $parcours 0] 1] [file tail $i]}"
    grid .framebottom.enonce$ind -row [expr int($ind/2)] -column [expr int(fmod($ind,2))]
    incr ind
  }
}



