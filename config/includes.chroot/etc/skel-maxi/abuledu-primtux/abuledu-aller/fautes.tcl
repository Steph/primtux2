#!/bin/sh
#fautes.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2002 Eric Seigne <erics@rycks.com>
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : $$$
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  Eric Seigne 
# 
#  *************************************************************************
global sysFont nbreu essais auto couleur aide longchamp xcol ycol listevariable categorie startdirect tableaumots user Homeconf locktext reussi repertoire tabaide tablistevariable tablongchamp tabstartdirect Home initrep baseHome lecture_mot lecture_mot_cache tablecture_mot tablecture_mot_cache

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}

#variables
#nbreu : nombre d'items effectues
#essais : total des essais effectues
#auto : flag de detection du mode de fonctionnement 
#couleur : couleur associ�e � l'exercice
#aide : pr�cise � quel moment doit intervenir l'aide
#longchamp : pr�cise si les champs sont de longueur variable

variable substexte
set substexte 1
set reussi 0
set nbreu 0
set essais 0
set auto 0
set couleur grey
set aide 2
set longchamp 1
set xcol 0
set ycol 0
set listevariable 1
set categorie ""
set startdirect 1
set locktext 0

set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface
. configure -background black -width 640 -height 480
wm geometry . +52+0


#wm title . Bilan\040[lindex [split [lindex [split $file /] end] .] 0]
frame .menu -height 40
pack .menu -side bottom -fill both
#bind .menu  <Destroy> "fin"
#button .menu.b1 -text [mc {Commencer}] -command "main .text"
button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main .text"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right
tux_commence

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both
#recup�ration des options de reglages
catch {
#set f [open [file join $baseHome reglages faute.conf] "r"]
#set aller [gets $f]
#close $f
#set aide [lindex $aller 0]
#set longchamp [lindex $aller 1]
#set listevariable [lindex $aller 2]
#set categorie [lindex $aller 3]
#set startdirect [lindex $aller 4]
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_fautes [lindex $aller 4]
close $f
set aide [lindex $aller_fautes 0]
set longchamp [lindex $aller_fautes 1]
set listevariable [lindex $aller_fautes 2]
#set categorie [lindex $aller_fautes 3]
set startdirect [lindex $aller_fautes 4]
set lecture_mot [lindex $aller_fautes 5]
set lecture_mot_cache [lindex $aller_fautes 6]
}
set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
if {$auto == 1} {
set aide $tabaide(faute)
set longchamp $tablongchamp(faute)
set listevariable $tablistevariable(faute)
set startdirect $tabstartdirect(faute)
set lecture_mot $tablecture_mot(faute)
set lecture_mot_cache $tablecture_mot_cache(faute)
}

#focus .text
#.text configure -state disabled

bind .text <ButtonRelease-1> "lire"
bind .text <Any-Enter> ".text configure -cursor target"
bind .text <Any-Leave> ".text configure -cursor left_ptr"

bind . <KeyPress> "cancelkey %A"

wm title . "[mc {Exercice}] $categorie - [lindex [lindex $listexo 4] 1]"
#label .menu.titre -text "[lindex [lindex $listexo 4] 1] - [mc {Observe}]" -justify center
#pack .menu.titre -side left -fill both -expand 1


proc main {t} {
#liste principale de phrases contenant les mots sans ponctuation, liste de mots � cacher
#listessai : tableau pour tenir � jour les essais sur chaque mot
#texte : le texte initial
#longmot : longueur maximale des champs de texte

global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot listexo iwish aide user startdirect
variable substexte

set nbmotscaches 0
#catch {destroy .menu.b1}

if {$substexte == 1} {
button .menu.b2 -image [image create photo corriger -file [file join sysdata corriger.gif]] -command "abandon $t"
#button .menu.b2 -text [mc {Correction}] -command "abandon $t"
pack .menu.b2 -side left
}
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
 
#button .menu.b1 -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"
#pack .menu.b1 -side right

$t configure -state normal
wm geometry .wtux +200+240

if {$substexte == 1} {
set what [mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree. Clique sur la bouee pour abandonner et obtenir la correction.}]
} else {
set what [mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]
}

set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"
tux_exo $what

# S�lection du mode auto ou manuel pour la g�n�ration de l'exercice
    if {$auto==0} {
    pauto $t
    } else {
    pmanuel $t
    }
catch {destroy .menu.lab}
label .menu.lab -text [lindex [lindex $listexo 4] 2] -justify center
#[mc {Clique sur les mots mal ecrits, reecris-les et appuie sur entree.}]
pack .menu.lab
    $t configure -state disabled -selectbackground white -selectforeground black
}

proc pauto {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches aide longchamp longmot listeaide startdirect iwish filuser lecture_mot lecture_mot_cache
set plist [parse $t]
set listemotscaches ""
set compte [comptemots $plist]

#on choisit le premier mot dans un rang al�atoire de 1 � 6
#on d�termine tout les combien environ on va cacher un mot
#on transforme la liste principale ( de phrases en une liste de mots)
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }


set nbmotscaches [expr int($compte/5)]
    if {$nbmotscaches >= 10} {
    set nbmotscaches 10
    }

if {$nbmotscaches == 0} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }


#on choisit le premier mot dans un rang al�atoire de 1 � 6
set indexpremiermot [expr int(rand()*5) + 1]
#on d�termine tout les combien environ on va cacher un mot
set frequence [expr int($compte/$nbmotscaches)]
#on transforme la liste principale ( de phrases en une liste de mots)
#on r�cup�re le premier mot � masquer
###lappend listemotscaches [lindex $tmp $indexpremiermot]
#on r�cup�re les autres mots � masquer, dont la longueur doit �tre sup � 3, sinon on cherche un autre mot en revenant en arri�re
    for {set i 1} {$i < $nbmotscaches} {incr i 1} {
    set mot [lindex $tmp [expr $indexpremiermot + $frequence*$i]]
    set index [expr $indexpremiermot + $frequence*$i]
        while {[string length $mot] <=3 && $index > [expr $indexpremiermot + $frequence*($i -1)]} {
        set index [expr $index -1]
        if {$index <= 0 } {break}
        set mot [lindex $tmp $index]
        }

if {[string length $mot] > 2 && $mot != " " && $mot != [lindex $listemotscaches end]} {
    lappend listemotscaches $mot
    }
}

set nbmotscaches [llength $listemotscaches]

set longmot 0
        foreach mot $listemotscaches {
            if {$longmot < [string length $mot]} {
            set longmot  [string length $mot]
            }
        }
#on recherche les mots cach�s dans le texte, on les supprime et on les remplace par une zone de saisie
    set re1 {\m}
    set re2 {\M} 
    set cur 0.0

    for {set i 0} {$i < $nbmotscaches} {incr i 1} {
    #set cur [$t search [lindex $listemotscaches $i] $cur end]
    set cur [$t search -regexp $re1[lindex $listemotscaches $i]$re2 $cur end]
    $t delete $cur "$cur + [string length [lindex $listemotscaches $i]] char"
    $t insert $cur [melange [lindex $listemotscaches $i]] ent$i

    if {$startdirect == 1} {
    $t tag add grey $cur "$cur + [string length [lindex $listemotscaches $i]] char"
    }
    set listessai($i) 0
    }
    $t tag configure grey -background grey
    $t tag configure rouge -background red
    #on marque le texte entier avec le tag text
    $t tag add text 1.0 end
    $t tag bind text <ButtonRelease-1> "hit $t"
    if {$lecture_mot_cache == "0" } {
    catch {destroy .menu.bb1}
   }
bind .text <ButtonRelease-1> ""
bind .text <Any-Enter> ""
bind .text <Any-Leave> ""
}

######################################################################################
proc hit {t} {
global sysFont longchamp longmot listemotscaches essais motfaux locktext auto tableauindices tableaumots lecture_mot_cache lecture_mot
variable substexte
if {$locktext == 0} {
$t configure -state normal
set ind [lsearch [$t tag names current] "ent*"]
    if {$ind != -1} {
    set nom [lindex [$t tag names current] $ind]
    set num [string map {ent ""} $nom]
    set motfaux [$t get $nom.first $nom.last]
    $t delete $nom.first $nom.last
        foreach tag [$t tag names current] {
        catch {$t tag remove $tag $nom.first $nom.last}
        }
    set longmot [string length $motfaux]
        if {$auto == 1 && $substexte == 0} {
           if {[string length $motfaux] < [string length $tableaumots($motfaux)]} {
           set longmot [string length $tableaumots($motfaux)]
           }
        }
        if {$auto == 1 && $substexte == 1} {
        set tmp ""
	     foreach item [array names tableaumots] {
              if {$tableaumots($item) == $motfaux} {
              set tmp $item
              break
              }
           }
           if {$tmp != ""} {
              if {[string length $tmp] > [string length $motfaux]} {
              set longmot [string length $tmp]
              }
           }
         }
    entry $t.$nom -font $sysFont(l) -width [expr $longmot +2] -bg yellow
    $t.$nom insert end $motfaux
    set locktext 1
    $t window create current -window $t.$nom
	tux_continuebon
        if {$auto == 1 && $substexte == 0} {
           catch { 
           if {$tableauindices($motfaux)!= " "} {
          .menu.lab configure -text "[mc {Indice :}] $tableauindices($motfaux)"
          }
        }
     }
        if {$auto == 1 && $substexte == 1} {
           catch { 
          if {$tmp != "" && $tableauindices($tmp)!= " "} {
          .menu.lab configure -text "[mc {Indice :}] $tableauindices($tmp)"
          }
        }
     }

    bind $t.$nom <Return> "verif $num $t"
if {$lecture_mot == "1" } {
speaktexte [lindex $listemotscaches $num]
}

    focus $t.$nom
    } else {
    $t tag add rouge current
    incr essais
	tux_echoue1
    }
$t configure -state disabled
}
}

########################################################################################"
proc pmanuel {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches couleur aide longchamp longmot listeaide tableaumots auto tableauindices startdirect lecture_mot_cache
variable substexte

set nbmotscaches 0
set listemots {}
set listemotscaches {}

# Construction de la liste des mots � cacher, � partir des tags
set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    incr nbmotscaches
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    #regsub -all \"+ $str "" str
    lappend listemotscaches $str 
    }

 set longmot 0
    foreach mot $listemotscaches {
         if {$longmot < [string length $mot]} {
         set longmot  [string length $mot]
         }
    }

#Si aucun mot n'a �t� masqu�, on repasse en mode auto.
    if {$nbmotscaches == 0} {
    set auto 0
    set substexte 1
    pauto $t
    return
    }
#On marque le texte pour substituer les mots � cacher par des champs de texte
    for {set i 0} {$i < [llength $liste]} {incr i 1} {
    $t mark set cur$i [lindex $liste $i]
    }

# On op�re la substitution
    if {$substexte == 0} {
        for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
        $t delete cur[expr $i*2] cur[expr ($i*2) +1]
        $t insert cur[expr $i*2] [lindex $listemotscaches $i] ent$i
#startdirect : variable r�cup�r�e pour d�termin�e si on marque les mots � corriger
            if {$startdirect == 1} {
            set cur [$t index ent$i.first]
            $t tag add grey $cur "$cur + [string length [lindex $listemotscaches $i]] char"
            }
            if {[catch {set tmp $tableaumots([lindex $listemotscaches $i])}] ==1} {
            set tableaumots([lindex $listemotscaches $i]) [lindex $listemotscaches $i]
            }
            if {[catch {set tmp $tableauindices([lindex $listemotscaches $i])}] ==1} {
            set tableauindices([lindex $listemotscaches $i]) " "
            }
         set listessai($i) 0
         }
     } else {
         for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {           
         $t delete cur[expr $i*2] cur[expr ($i*2) +1]
             if {[catch {set tmp $tableaumots([lindex $listemotscaches $i])}] !=1} {
             $t insert cur[expr $i*2] $tmp ent$i
		set wordlong [string length $tmp] 
             } else {
             $t insert cur[expr $i*2] [melange [lindex $listemotscaches $i]] ent$i
		set wordlong [string length [lindex $listemotscaches $i]] 
             }
            if {$startdirect == 1} {
            set cur [$t index ent$i.first]
            $t tag add grey $cur "$cur + $wordlong char"
            }

          set listessai($i) 0
          }

     }    


    $t tag configure grey -background grey
    $t tag configure rouge -background red
    #on marque le texte entier avec le tag a
    $t tag add text 1.0 end
    $t tag bind text <ButtonRelease-1> "hit $t"
    $t tag bind text <ButtonRelease-1> "hit $t"
    if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1} 
  bind .text <ButtonRelease-1> ""

}
}


proc verif {i t} {
global sysFont listemotscaches listessai nbreu essais aide listeaide listevariable motfaux nbmotscaches locktext tableaumots auto global reussi listexo disabledfore disabledback
variable substexte
incr essais

switch $substexte {
1 {set tverif [lindex $listemotscaches $i]
   set terreur $motfaux}
0 {set tverif $tableaumots([lindex $listemotscaches $i])
   set terreur [lindex $listemotscaches $i]}
}
    if {$tverif == [$t.ent$i get]} {
    incr nbreu
    incr reussi
    if {$listessai($i) == 0} {tux_reussi}
    if {$listessai($i) >= 1}  {tux_continue_bien}
    catch {destroy .menu.lab}
    label .menu.lab -text "[lindex [lindex $listexo 4] 2] - [format [mc {%1$s mot(s) sur %2$s.}] $nbreu $nbmotscaches]"
    pack .menu.lab

    bind $t.ent$i <Return> {}
	set locktext 0
    $t.ent$i configure -state disabled -$disabledfore blue
    } else {
    $t.ent$i delete 0 end
    $t.ent$i insert end $terreur
if {$listessai($i) >= 1} {tux_echoue2} else {tux_echoue1}
        if {[incr listessai($i)] >= [expr $aide] } {
        incr nbreu
        $t.ent$i delete 0 end
        $t.ent$i insert end $tverif
        $t.ent$i configure -state disabled
        bind $t.ent$i <Return> {}
	  set locktext 0
	  catch {destroy .menu.lab}
        label .menu.lab -text "[lindex [lindex $listexo 4] 2] - [format [mc {%1$s mot(s) sur %2$s.}] $nbreu $nbmotscaches]"
        pack .menu.lab
        }
    }
testefin $nbreu [llength $listemotscaches] $essais $reussi
affichecouleur $i $t
}

proc testefin {nbreu total essais reussi} {
global sysFont user categorie listemotscaches
variable substexte
    if {$nbreu >= $total} {
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

    catch {destroy .menu.lab}

    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $total]

    set str1 [mc {Exercice Texte a corriger}]
    label .menu.lab -text $str0$str2
    pack .menu.lab -side right
    .text tag remove text 1.0 end
    bell
       catch {destroy .menu.b2}

       if {[expr int($reussi*100/$total)] > 60 && $substexte == 0} {
	 button .menu.b2
       pack .menu.b2 -side left
       .menu.b2 configure -text [mc {Enregistrer le texte corrige}] -command "sauvecorrige \173$categorie\175"

       }     }
}

proc melange {mot} {
set copie $mot
set long [string length $mot]
if {$long <=1 } {return $mot}
while {$mot==$copie} {
  for {set i 0} {$i < [expr $long*2]} {incr i 1} {
  set t1 [expr int(rand()*$long)]
  set t2 [expr int(rand()*$long)]
  set tmp1 [string index $mot $t1]
  set tmp2 [string index $mot $t2]
  set mot [string replace $mot $t1 $t1 $tmp2]
  set mot [string replace $mot $t2 $t2 $tmp1]
  }
}
  return $mot
}


proc affichecouleur {ind t} {
global sysFont listessai disabledfore disabledback

switch $listessai($ind) {
    0 { $t.ent$ind configure -$disabledback yellow -bg yellow}
    1 { $t.ent$ind configure -$disabledback green -bg green}
    default { $t.ent$ind configure -$disabledback red -bg red}
    }
}

#############################################################################"
#if {$startdirect == 0 } {
main .text
#}
#############################################################################
proc abandon {t} {
global sysFont plist listemotscaches listessai texte nbmotscaches auto longchamp longmot tableaumots essais motfaux disabledback nbreu
variable substexte
set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}

set cur 0.0
$t configure -state normal
catch {destroy .menu.b2}
catch {destroy .menu.lab}

set listtag [$t tag names]
    foreach tg $listtag {
catch {
       set ind [lsearch $tg "ent*"]
       if {$ind != -1} {

          set nom $tg
          set num [string map {ent ""} $nom]
          set motfaux [$t get $nom.first $nom.last]
          set cur [$t index $nom.first]

          $t delete $nom.first $nom.last
	   switch $substexte {
         1 {$t insert $cur [lindex $listemotscaches $num]
          $t tag add violet $cur "$cur + [string length [lindex $listemotscaches $num]] char"
           }
         0 {$t insert $cur $tableaumots([lindex $listemotscaches $num])
           $t tag add violet $cur "$cur + [string length $tableaumots([lindex $listemotscaches $num])] char"}
	   }

              
        $t tag bind text <ButtonRelease-1> {}

       }
}
}
$t tag configure violet -background pink

$t configure -state disabled

}

proc fin {} {
global sysFont categorie user essais listemotscaches nbreu listexo iwish filuser aide startdirect repertoire lecture_mot lecture_mot_cache
variable repertconf
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches] ]
    set str1 [mc {Exercice Texte a corriger}]
    #set score [expr ($nbreu*100)/$essais]
    set score [expr ($nbreu*100)/([llength $listemotscaches] +($essais- $nbreu))]
switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $aide {
1 { set aideconf [mc {Au debut}]}
2 { set aideconf [mc {Apres le premier essai}]}
3 { set aideconf [mc {Apres le deuxieme essai}]}
}

switch $lecture_mot_cache {
1 { set lectmotconf [mc {Le texte peut �tre entendu.}]}
0 { set lectmotconf [mc {Le texte ne peut pas �tre entendu.}]}
}

switch $lecture_mot {
1 { set lectmotcacheconf [mc {Les mots peuvent �tre entendus.}]}
0 { set lectmotcacheconf [mc {Les mots ne peuvent pas �tre entendus.}]}
}

set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf Aide : $aideconf"
set exoconf "$exoconf  Son : $lectmotconf $lectmotcacheconf"

    enregistreval $str1\040[lindex [lindex $listexo 4] 1] \173$categorie\175 $str2 $score $repertconf 4 $user $exoconf $repertoire

exec $iwish aller.tcl $filuser &
exit
}

proc sauvecorrige {categorie} {
global tableaumots Home
set nbmotscaches 0
set listemotscaches {}
.text configure -state normal
.text delete 1.0 end
charge .text $categorie
set liste [.text tag ranges grey]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    incr nbmotscaches
    set str [.text get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    lappend listemotscaches $str 
    }
#On marque le texte pour substituer les mots � cacher par des champs de texte
    for {set i 0} {$i < [llength $liste]} {incr i 1} {
    .text mark set cur$i [lindex $liste $i]
    }

# On op�re la substitution
        for {set i 0} {$i < [llength $listemotscaches]} {incr i 1} {
        .text delete cur[expr $i*2] cur[expr ($i*2) +1]
        if {[catch {set tmp $tableaumots([lindex $listemotscaches $i])}] !=1} {
            .text insert cur[expr $i*2] $tmp ent$i
          } 
        }


set texte [.text get 1.0 "end - 1 chars"]
#regsub -all \" $texte \\\" texte

set types {    {{Fichiers texte}            {.txt}        }}
set ext .txt
catch {set name [tk_getSaveFile -filetypes $types -initialdir $Home]}
    if {$name != ""} {
        if  {[string match -nocase *.txt $name]==0} {
        set name $name$ext
        }

    set f [open $name "w"]  
    puts $f $texte
    close $f
    }
.text configure -state disabled
catch {destroy .menu.b2}
}

proc boucle {} {
global sysFont categorie user essais listemotscaches nbreu tableaumots tableauindices listexo auto locktext
    set str0 [mc {Exercice termine en }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s) sur %3$s.}] $essais $nbreu [llength $listemotscaches] ]
    set str1 [mc {Exercice Texte a corriger}]
    enregistreval $str1 \173$categorie\175 $str2 $user
set essais 0
set nbreu 0
set locktext 0
catch {unset tableaumots}
catch {unset tableauindices}
set listexo ""
set listemotscaches ""
set categorie "Au choix"
.text configure -state normal -font $sysFont(l)
.text tag delete all
set auto [charge .text $categorie]
.text configure -state disabled
focus .text
catch {destroy .menu.suiv}
catch {destroy .menu.lab}
catch {destroy .menu.b2}
catch {destroy .menu.b1}

main .text
}
