set etapes 1
set niveaux {0 1}
::1
set niveau 1
set ope {{1 9} {1 9}}
set interope {{1 10 1} {1 10 1}}
set ope1 [expr int(rand()*[lindex [lindex $ope 0] 1]) + [lindex [lindex $ope 0] 0]]
set ope2 [expr int(rand()*[lindex [lindex $ope 1] 1]) + [lindex [lindex $ope 1] 0]]
set volatil 0
set operations {{0+0}}
set editenon "(Il a [expr $ope1*10 + $ope2] perles.)"
set enonce "Les perles.\nTom a une belle collection de perles rouges : \nTim veut avoir autant de perles que Tom.\nChoisis les dominos et les perles qu'il doit prendre.\nTu peux cliquer sur les perles rouges pour les compter."
set cible {{3 3 {} source0} {3 3 {} source1}}
set intervalcible 60
set taillerect 70
set orgy 50
set orgxorig 50
set orgsourcey 100
set orgsourcexorig 600
set source {carte.gif perle.gif}
set orient 0
set labelcible {{Dominos de Tim} {Perles de Tim}}
set quadri 0
set ensembles [list [expr $ope1] [expr $ope2]]
set reponse [list [list {1 3} [list {Il faut} [expr $ope1] {domino(s) et} [expr $ope2] perle(s).]]]
set dessin [expr $ope1*10 + $ope2]
set canvash 300
set c1height 160
set opnonautorise {}
::
