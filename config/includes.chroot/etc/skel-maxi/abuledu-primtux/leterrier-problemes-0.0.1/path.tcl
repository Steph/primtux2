#!/bin/sh
#path.tcl
# \
exec wish "$0" ${1+"$@"}

#*************************************************************************
#  Copyright (C) 2007 David Lucardi
# 
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
# 
#**************************************************************************
#  File  : path.tcl
#  Author  : davidlucardi@aol.com
#  Modifier: 
#  Date    : 
#  Licence : GNU/GPL Version 2 ou plus
# 
#  Description:
#  ------------
# 
#  @version    $Id: path.tcl,v 1.6 2005/05/05 04:43:43 david Exp $
#  @author     David Lucardi
#  @modifier   
#  @project    Le terrier
#  @copyright  David Lucardi 
# 
#  *************************************************************************

package require Img

proc setwindowsusername {} {
    global user LogHome
    catch {destroy .utilisateur}
    toplevel .utilisateur -background grey -width 250 -height 100
    wm geometry .utilisateur +50+50
    frame .utilisateur.frame -background grey -width 250 -height 100
    pack .utilisateur.frame -side top
    label .utilisateur.frame.labobj -font {Helvetica 10} -text [mc {Quel est ton nom?}] -background grey
    pack .utilisateur.frame.labobj -side top 

    listbox .utilisateur.frame.listsce -yscrollcommand ".utilisateur.frame.scrollpage set" -width 15 -height 10
    scrollbar .utilisateur.frame.scrollpage -command ".utilisateur.frame.listsce yview" -width 7
    pack .utilisateur.frame.listsce .utilisateur.frame.scrollpage -side left -fill y -expand 1 -pady 10
    bind .utilisateur.frame.listsce <ButtonRelease-1> "verifnom %x %y"
    foreach i [lsort [glob [file join $LogHome *.log]]] {
    .utilisateur.frame.listsce insert end [string map {.log \040} [file tail $i]]
    }

    button .utilisateur.frame.ok -background gray75 -text [mc {Ok}] -command "interface; destroy .utilisateur"
    pack .utilisateur.frame.ok -side top -pady 70 -padx 10
}

proc verifnom {x y} {
    global env user LogHome filuser baseHome

set ind [.utilisateur.frame.listsce index @$x,$y]
set nom [string trim [.utilisateur.frame.listsce get $ind]]
    if {$nom !=""} {
	set env(USER) $nom
	set filuser $nom.conf

	if {! [file exists [file join $baseHome reglages $filuser]]} {
	set filuser problemes.conf
	}

	set user [file join $LogHome $nom.log]
    }
}

proc init {plateforme} {
global Home basedir baseHome

if {![file exists [file join $Home]]} {
	file mkdir [file join $Home]
	file copy -force [file join problemes] [file join $Home]
	file copy -force [file join reglages] [file join $Home]
	}




switch $plateforme {
    unix {
	if {![file exists [file join  problemes log]]} {
	file mkdir [file join  problemes log]
	}

    }
    windows {
	if {![file exists [file join problemes log]]} {
	file mkdir [file join problemes log]
	}
	
    	}
}

}


proc initlog {plateforme ident} {
global LogHome user Home baseHome
switch $plateforme {
    unix {
	set LogHome [file join $Home problemes log]

    }
    windows {
	set LogHome [file join problemes log]
    }
}

if {$ident != ""} {
     set user [file join $LogHome $ident.log]
     } else {
     set user [file join $LogHome problemes.log]
     }
if {![file exists [file join $user]]} {
set f [open [file join $user] "w"]
close $f
}
}

proc inithome {} {
global baseHome basedir Homeconf Home filuser
variable repert
##set f [open [file join $baseHome reglages repert.conf] "r"]
##set repert [gets $f]
##close $f
set repert 0
switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}
}


proc changehome {} {
global Home basedir baseHome Homeconf filuser
variable repert
set f [open [file join $baseHome reglages repert.conf] "w"]
puts $f $repert
close $f

switch $repert {
0 {set Home $baseHome}
1 {set Home $basedir }
}
}

global basedir Home baseHome iwish progaide envir abuledu prof filuser
set filuser problemes.conf
set basedir [pwd]
cd $basedir
if {$env(HOME) == "c:\\"} {
    set Home [file join $basedir]
    set Homeconf [file join $basedir]
    
} else {
    set Home [file join $env(HOME) leterrier problemes]
    set Homeconf [file join $env(HOME) leterrier problemes]
}
set baseHome $Home

switch $tcl_platform(platform) {
    unix {
	if {[lsearch [exec id -nG $env(USER)] "leterrier"] != -1} {set prof 1}
	#if {[file isdirectory [file join /etc abuledu]]} { set abuledu 1}
	set abuledu 1
	set progaide runbrowser
	set iwish wish
	}
    windows {
	set progaide shellexec.exe
	set iwish wish
	}
	}


