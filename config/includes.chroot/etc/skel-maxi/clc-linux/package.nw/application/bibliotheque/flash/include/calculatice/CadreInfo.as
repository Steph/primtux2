﻿class CadreInfo {
	public var msg_norep:String;
	public var msg_fpe:String;
	public var msg_fde:String;
	public var msg_fdq:String;
	public var msg_j:String;
	public var msg_jdq:String;
	private var objExo;
	private var cadre_info:MovieClip;
	private var texte_info:TextField;
	private var texte:String;
	private var format_texte:TextFormat;
	private var largeur:Number;
	private var x_info:Number;
	private var y_info:Number;
	public function CadreInfo(obj) {
		this.objExo=obj
		this.largeur = 325;
		this.format_texte=new TextFormat();
		this.format_texte.size=16;
		this.format_texte.bold=true;
		this.format_texte.font="emb_tahoma";
		this.format_texte.align="center";
		this.cadre_info = objExo.rootMovie.createEmptyMovieClip("cadre_info", objExo.rootMovie.getNextHighestDepth());
		this.cadre_info._x=390;
		this.cadre_info._y=310;
		this.msg_norep=objExo.GetTradCommun("p3_1");//message pas de reponse
		this.msg_j=objExo.GetTradCommun("p3_2");//message juste
		this.msg_jdq=objExo.GetTradCommun("p3_3");//message juste derniere question
		this.msg_fpe=objExo.GetTradCommun("p3_5");//message faux premier essai
		this.msg_fde=objExo.GetTradCommun("p3_4");//message faux dernier essai
		this.msg_fdq=objExo.GetTradCommun("p3_6")//message faux dernier essai dernière question
	}
	public function SetInfo(texte:String) {
		this.texte=texte;
		if (this.texte_info == undefined) {
			var d = this.cadre_info.getNextHighestDepth();
			this.texte_info = this.cadre_info.createTextField("texte_info", d, 5, 5, this.largeur-20,20);
			this.texte_info.embedFonts=true;
			this.texte_info.antiAliasType = "advanced";
		}
		
		this.texte_info.multiline = true;
		this.texte_info.wordWrap = true;
		this.texte_info.autoSize = true;
		this.texte_info.text = texte;
		this.texte_info.setTextFormat(this.format_texte);
		
		var largeur = this.largeur;
		var hauteur = this.texte_info._height+10;
		var rayon = 10;
		var couleur1 = 0xCCCCCC;
		var couleur2 = 0xDFE4FF;
		
		this.cadre_info.clear();
		this.cadre_info.lineStyle(1, couleur1);
		this.cadre_info.beginFill(couleur2);
		drawRect(this.cadre_info,0,0,largeur,hauteur,rayon);
		this.cadre_info.endFill();
	}
	//public function SetFormatCadre(x_cadre:Number, y_cadre:Number, largeur_cadre:Number){
		//this.cadre_info._x=x_cadre;
		//this.cadre_info._y=y_cadre;
	public function SetWidth(largeur_cadre:Number){
		this.largeur=largeur_cadre;
		if (this.texte_info != undefined){
			SetInfo(this.texte)
		}
		
	}
	public function SetPos(x:Number,y:Number){
		this.cadre_info._x = x;
		this.cadre_info._y = y;
	}
	/*
	public function SetFormatTexte(fmt:TextFormat){
		this.format_texte=fmt;
		if (this.texte_info != undefined){
			SetInfo(this.texte)
		}
	}
	*/
	public function Hide() {
		this.cadre_info._visible = false;
	}
	public function Show() {
		this.cadre_info._visible = true;
	}
	
	/*-------------------------------------------------------------
	mc.drawRect is a method for drawing rectangles and
	rounded rectangles. Regular rectangles are
	sufficiently easy that I often just rebuilt the
	method in any file I needed it in, but the rounded
	rectangle was something I was needing more often,
	hence the method. The rounding is very much like
	that of the rectangle tool in Flash where if the
	rectangle is smaller in either dimension than the
	rounding would permit, the rounding scales down to
	fit.
	-------------------------------------------------------------*/
	private function drawRect(targetClip, x, y, w, h, cornerRadius) {
		// ==============
		// mc.drawRect() - by Ric Ewing (ric@formequalsfunction.com) - version 1.1 - 4.7.2002
		// 
		// x, y = top left corner of rect
		// w = width of rect
		// h = height of rect
		// cornerRadius = [optional] radius of rounding for corners (defaults to 0)
		// ==============
		if (arguments.length<4) {
			return;
		}
		// if the user has defined cornerRadius our task is a bit more complex. :) 
		if (cornerRadius>0) {
			// init vars
			var theta, angle, cx, cy, px, py;
			// make sure that w + h are larger than 2*cornerRadius
			if (cornerRadius>Math.min(w, h)/2) {
				cornerRadius = Math.min(w, h)/2;
			}
			// theta = 45 degrees in radians 
			theta = Math.PI/4;
			// draw top line
			targetClip.moveTo(x+cornerRadius, y);
			targetClip.lineTo(x+w-cornerRadius, y);
			//angle is currently 90 degrees
			angle = -Math.PI/2;
			// draw tr corner in two parts
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw right line
			targetClip.lineTo(x+w, y+h-cornerRadius);
			// draw br corner
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+w-cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+w-cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw bottom line
			targetClip.lineTo(x+cornerRadius, y+h);
			// draw bl corner
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+h-cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+h-cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			// draw left line
			targetClip.lineTo(x, y+cornerRadius);
			// draw tl corner
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
			angle += theta;
			cx = x+cornerRadius+(Math.cos(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			cy = y+cornerRadius+(Math.sin(angle+(theta/2))*cornerRadius/Math.cos(theta/2));
			px = x+cornerRadius+(Math.cos(angle+theta)*cornerRadius);
			py = y+cornerRadius+(Math.sin(angle+theta)*cornerRadius);
			targetClip.curveTo(cx, cy, px, py);
		} else {
			// cornerRadius was not defined or = 0. This makes it easy.
			targetClip.moveTo(x, y);
			targetClip.lineTo(x+w, y);
			targetClip.lineTo(x+w, y+h);
			targetClip.lineTo(x, y+h);
			targetClip.lineTo(x, y);
		}
	}
}
