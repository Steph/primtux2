var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.sommeenligne = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var i,nombreA,nombreB,uniteA,uniteB,dizaineA,dizaineB,retenueUnite,retenueDizaine;
var aNombre, aMultiple10, soluce, texte;
var cartouche, champReponse;

// Référencer les ressources de l'exercice (images, sons)
exo.oRessources = { 
    illustration:"sommeenligne/images/illustration.png",
    cartouche:"sommeenligne/images/cartouche.png"
};

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
        totalQuestion:20,
        totalEssai:1,
        tempsQuestion:0,
        tempsExo:0,
        temps_expo:20,
        typeAddition:0,
        avecRetenue:0
    };
    $.extend(exo.options,optionsParDefaut,oOptions);
};

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
//Tirage des nombres en fonction des paramètres nombre_a et nombre_b
    aNombre = [];//On initialise aNombre
    aMultiple10 = [];//On initialise aMultiple10
    if(exo.options.typeAddition===0 && exo.options.avecRetenue===0) {
        //2 chiffres + 1 chiffre res <50 sans retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 10 + Math.floor(Math.random()*(40-10+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            if ((uniteA+uniteB < 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(nombreA + nombreB <= 50)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            }
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition === 0 && exo.options.avecRetenue == 1) {
        //2 chiffres + 1 chiffre res < 50 avec retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 10 + Math.floor(Math.random()*(40-10+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            if ((uniteA+uniteB >= 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(nombreA + nombreB <= 50)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition == 1 && exo.options.avecRetenue === 0) {
        //2 chiffres + 1 chiffre res < 100 sans retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 50 + Math.floor(Math.random()*(90-50+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            if ((nombreA + nombreB < 100)&&(uniteA+uniteB < 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==1 && exo.options.avecRetenue==1) {
	//2 chiffres + 1 chiffre res < 100 avec retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 50 + Math.floor(Math.random()*(100-50+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            if ((uniteA+uniteB >= 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(nombreA + nombreB <= 100)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            }
            else {
                if(nombreA % 10 === 0){
                    aMultiple10.shift();
                }
                i--;
            }
        }
    }
    else if(exo.options.typeAddition==2 && exo.options.avecRetenue === 0) {
        //2 chiffres + 2 chiffres sans retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 10 + Math.floor(Math.random()*(99-10+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(99-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10);
            dizaineB = Math.floor(nombreB/10);
            if ((uniteA+uniteB < 10)&&(dizaineA + dizaineB < 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==2 && exo.options.avecRetenue==1) {
	//2 chiffres + 2 chiffres avec 1 seule retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 10 + Math.floor(Math.random()*(89-10+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(89-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10);
            dizaineB = Math.floor(nombreB/10);
            retenueUnite = Math.floor((uniteA+uniteB)/10);
            retenueDizaine = Math.floor((dizaineA+dizaineB+retenueUnite)/10);
            if ((retenueUnite > 0  || retenueDizaine > 0 )&&!(retenueUnite > 0 && retenueDizaine > 0)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)){
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==2 && exo.options.avecRetenue==2) {
        //2 chiffres + 2 chiffres avec 2 retenues
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 10 + Math.floor(Math.random()*(89-10+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(89-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10);
            dizaineB = Math.floor(nombreB/10);
            retenueUnite = Math.floor((uniteA+uniteB)/10);
            retenueDizaine = Math.floor((dizaineA+dizaineB+retenueUnite)/10);
            if ((retenueUnite > 0  && retenueDizaine > 0 )&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition == 3 && exo.options.avecRetenue === 0) {
        //3 chiffres + 1 chiffres sans retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 100 + Math.floor(Math.random()*(500-100+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            if ((uniteA+uniteB < 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
               aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==3 && exo.options.avecRetenue>0) {
        //3 chiffres + 1 chiffres avec 1 seule retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 100 + Math.floor(Math.random()*(700-100+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 2 + Math.floor(Math.random()*(9-2+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10)%10;
            dizaineB = Math.floor(nombreB/10);
            retenueUnite = Math.floor((uniteA+uniteB)/10);
            retenueDizaine = Math.floor((dizaineA+dizaineB+retenueUnite)/10);
            if ((retenueUnite > 0  || retenueDizaine > 0 )&& !(retenueUnite > 0 && retenueDizaine > 0)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            }
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    }
    else if(exo.options.typeAddition == 4 && exo.options.avecRetenue === 0) {
        //3 chiffres + 2 chiffres sans retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 100 + Math.floor(Math.random()*(800-100+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(99-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10)%10;
            dizaineB = Math.floor(nombreB/10);
            if ((uniteA+uniteB < 10)&&(dizaineA + dizaineB < 10)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==4 && exo.options.avecRetenue==1) {
        //3 chiffres + 2 chiffres avec une seule retenue
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 100 + Math.floor(Math.random()*(800-100+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(89-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10)%10;
            dizaineB = Math.floor(nombreB/10);
            retenueUnite = Math.floor((uniteA+uniteB)/10);
            retenueDizaine = Math.floor((dizaineA+dizaineB+retenueUnite)/10);
            if ((retenueUnite > 0  || retenueDizaine > 0 )&& !(retenueUnite > 0 && retenueDizaine > 0)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            } 
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    } 
    else if(exo.options.typeAddition==4 && exo.options.avecRetenue==2) {
        for(i=0;i<exo.options.totalQuestion;i++) {
            nombreA = 100 + Math.floor(Math.random()*(800-100+1));
            if(nombreA % 10 === 0) {
                aMultiple10.push(nombreA);
            }
            nombreB = 10 + Math.floor(Math.random()*(89-10+1));
            uniteA = nombreA % 10;
            uniteB = nombreB % 10;
            dizaineA = Math.floor(nombreA/10)%10;
            dizaineB = Math.floor(nombreB/10);
            retenueUnite = Math.floor((uniteA+uniteB)/10);
            retenueDizaine = Math.floor((dizaineA+dizaineB+retenueUnite)/10);
            if ((retenueUnite > 0  && retenueDizaine > 0)&&(aNombre.indexOf([nombreA,nombreB])<0)&&(aMultiple10.length<3)) {
                aNombre.push([nombreA,nombreB]);
            }
            else {
                if(nombreA % 10 === 0) {
                    aMultiple10.shift();
                }
                i--;
            }
        }
    }
};
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html("Somme en ligne");
    exo.blocConsigneGenerale.html("Donne le résultat d'une addition. Attention ! Tu ne pourras écrire ta réponse qu'une fois le calcul disparu.");
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration);
};

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;
    var temps_expo = exo.options.temps_expo *100;

    texte = aNombre[q][0] + " + " +aNombre[q][1] + " = ?";
    soluce = aNombre[q][0] + aNombre[q][1];

    //
    //
    cartouche = disp.createImageSprite(exo,"cartouche");
    exo.blocAnimation.append(cartouche);
    if(q%3 === 0) {
        cartouche.position({
            my:'left center',
            at:'right center',
            of:exo.blocAnimation
        });
    } 
    else if (q%3 == 1){
        cartouche.position({
            my:'right center',
            at:'left center',
            of:exo.blocAnimation
        });
    } 
    else {
        cartouche.position({
            my:'center bottom',
            at:'center top',
            of:exo.blocAnimation
        });        
    }
    //
    var etiquette = disp.createTextLabel(texte);
    etiquette.css({
        fontSize:32,
        fontWeight:'bold',
        color:'#fff'
    });
    cartouche.append(etiquette);
    etiquette.position({
        my:'center center',
        at:'center center',
        of:cartouche
    });
    champReponse = disp.createTextField(exo,6);
    if(q%3 === 0) {
        cartouche.animate({left:242},800,'easeInCubic',function(e){
            var to = setTimeout(degageCartouche,temps_expo);
        });
        function degageCartouche() {
            cartouche.animate({top:-800},800,'easeInCubic',function(e){
                exo.blocAnimation.append(champReponse);
                champReponse.position({
                    my:'center center',
                    at:'center center',
                    of:exo.blocAnimation
                });
                champReponse.focus();
                exo.btnValider.afficher();
            });
        }
    } 
    else if(q%3==1) {
        cartouche.animate({left:+242},800,'easeInCubic',function(e){
            var to = setTimeout(degageCartouche,temps_expo);
        });
        function degageCartouche() {
            cartouche.animate({top:-800},800,'easeInCubic',function(e){
                exo.blocAnimation.append(champReponse);
                champReponse.position({
                    my:'center center',
                    at:'center center',
                    of:exo.blocAnimation
                });
                champReponse.focus();
                exo.btnValider.afficher();
            });
        }
        
    } 
    else {
        cartouche.transition({top:195},800,'ease-in',function(e){
            var to = setTimeout(degageCartouche,temps_expo);
        });
        
    }
	
	function degageCartouche() {
            cartouche.transition({top:-800},800,'ease-in',function(e){
                exo.blocAnimation.append(champReponse);
                champReponse.position({
                    my:'center center',
                    at:'center center',
                    of:exo.blocAnimation
                });
                champReponse.focus();
                exo.btnValider.afficher();
            });
        }        
};

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
    if (champReponse.val() == "") {
       return "rien" 
    } else if( Number(champReponse.val()) == soluce ) {
        return "juste";
    } else {
        return "faux";
    }
}

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
    var correction = disp.createCorrectionLabel(soluce);
    exo.blocAnimation.append(correction)
    correction.position({
        my:"left center",
        at:"right+20 center",
        of:champReponse,
    })
    var barre = disp.drawBar(champReponse);
    exo.blocAnimation.append(barre);
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:32,
        taille:2,
        nom:"totalQuestion",
        texte:"Nombre de questions : "
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:32,
        taille:2,
        nom:"temps_expo",
        texte:"Temps d'exposition (en dixièmes de secondes) : "
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeAddition",
        texte:"Type d'addition :",
        aValeur:[0,1,2,3,4],
        aLabel:["2 chiffres + 1 chiffre, résultat < 50",
            "2 chiffres + 1 chiffre, résultat < 100",
            "2 chiffres + 2 chiffres",
            "3 chiffres + 1 chiffre",
            "3 chiffres + 2 chiffres"]
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"avecRetenue",
        texte:"Retenue : ",
        aValeur:[0,1,2],
        aLabel:["Sans", "Une seule", "Deux"]
    });
    exo.blocParametre.append(controle);
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))