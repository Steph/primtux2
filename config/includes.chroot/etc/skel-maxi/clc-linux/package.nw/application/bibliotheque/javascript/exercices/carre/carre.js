var CLC = (function(clc) {
//Ne pas oublier de renommer ci-dessous clc.xxxxx par clc.nom-de-votre-exercice
clc.carre = function(oOptions,path){
//
var exo = clc.exoBase(path);
var util = clc.utils;//raccourci
var disp = clc.display;//raccourci
var anim = clc.animation;//raccourci
    
// Déclarer ici les variables globales à l'exercice.

var aCible=[], aTemp=[], aNombre=[], aSoluce=[], aSoluceNombreCible=[], aCartouche=[];
var i,j,nA,nB,nC,nD,min,max;
var reponseEleve;
var cartouche;

// Référencer les ressources de l'exercice (images, sons)
// {
//      nom-de-la-ressource : chemin-du-fichier
// }

exo.oRessources = {
	txt:"carre/textes/carre_fr.json",
    illustration:"carre/images/illustration.png",
    cartouche:"carre/images/cartouche.png"
}

// Options par défaut de l'exercice (définir au moins exoName, totalQuestion, tempsExo et tempsQuestion)

exo.creerOptions = function() {
    var optionsParDefaut = {
	totalQuestion:10,
	totalEssai:1,
	tempsQuestion:10,
	typeSoluce:0,
	plageCible:"10-20"
    }
    $.extend(exo.options,optionsParDefaut,oOptions);
}

// Création des données de l'exercice (peut rester vide), exécutée a chaque fois que l'on commence ou recommence l'exercice

exo.creerDonnees = function() {
//Creation des donnees numeriques de l'exo
//
    aSoluceNombreCible= [];
    if(exo.options.typeSoluce==0) {
	aCible = util.getArrayNombre(exo.options.plageCible);
	aCible.sort(function(){return Math.floor(Math.random()*3)-1});
        console.log(aCible + " " + exo.options.plageCible);
	for(i=0;i<10;i++) {
            min = 5;
            max = aCible[i] - 2;
            nA =  min + Math.floor(Math.random()*(max-min+1));
            nB = aCible[i] - nA;
            while(true) {
		nC=min + Math.floor(Math.random()*(max-min+1));
		if(nC!=nA && nC!=nB) {
                    break;
                }
            }
            while(true) {
		nD=min + Math.floor(Math.random()*(max-min+1));
		if(nD!=nA && nD!=nB && nD!=nC) {
                    break;
                }
            }
            aTemp=new Array(nA,nB,nC,nD);
            aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            aSoluceNombreCible.push(new Array(new Array(nA,nB),aTemp,aCible[i]));
            console.log(aSoluceNombreCible);
	}	
    } else if(exo.options.typeSoluce==1) {
	//2 ou 3 cases a cliquer
	aCible = util.getArrayNombre(exo.options.plageCible);
	aCible.sort(function(){return Math.floor(Math.random()*3)-1});
        console.log(aCible + " " + exo.options.plageCible);
	//d'abord 5 solutions a trois cases
	for(i=0;i<5;i++) {
            min = 2;
            max = aCible[i] - 2;
            while(true) {
		nA=min + Math.floor(Math.random()*(max-min+1));
		nB=min + Math.floor(Math.random()*(max-min+1));
		nC=min + Math.floor(Math.random()*(max-min+1));
		if(nA!=nB && nA!=nC && nB!=nC && aCible[i]==nA+nB+nC) {
                    break;
		}
            }
            while (true) {
                nD = min + Math.floor(Math.random()*(max-min+1));
		if(nD!=nA && nD!=nB && nD!=nC) {
                    break;
		}
            }
            aTemp=new Array(nA,nB,nC,nD);
            aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            aSoluceNombreCible.push(new Array(new Array(nA,nB,nC),aTemp,aCible[i]));
	}
	// Ensuite 5 solutions a deux cases
	for(i=5;i<10;i++) {
            min = 5;
            max = aCible[i] - 2;
            nA =  min + Math.floor(Math.random()*(max-min+1));
            nB = aCible[i] - nA;
            while(true) {
                nC=min + Math.floor(Math.random()*(max-min+1));
                if(nC!=nA && nC!=nB) {
                    break;
		}
            }
            while(true) {
                nD=min + Math.floor(Math.random()*(max-min+1));
                if(nD!=nA && nD!=nB && nD!=nC) {
                    break;
		}
            }
            //aSoluce.push(new Array(nA,nB));
            aTemp=new Array(nA,nB,nC,nD);
            aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            aSoluceNombreCible.push(new Array(new Array(nA,nB),aTemp,aCible[i]));
            //aNombre.push(aTemp);
	}
	//enfin on melange
	aSoluceNombreCible.sort(function(){return Math.floor(Math.random()*3)-1});
    } else if(exo.options.typeSoluce==2) {
	aCible = util.getArrayNombre(exo.options.plageCible);
	aCible.sort(function(){return Math.floor(Math.random()*3)-1});
        console.log(aCible + " " + exo.options.plageCible);
	for(i=0;i<10;i++) {
            min = 2;
            max = 10;
            //nA =  min + Math.floor(Math.random()*(max-min+1));
            //nB = aCible[i] - nA;
            while(true) {
		nA =  min + Math.floor(Math.random()*(max-min+1));
		nB =  min + Math.floor(Math.random()*(max-min+1));
                if(nA+nB == aCible[i]) {
                    break;
		}
            }
            while(true) {
		nC=min + Math.floor(Math.random()*(max-min+1));
		if(nC!=nA && nC!=nB) {
                    break;
		}
            }
            while(true) {
		nD=min + Math.floor(Math.random()*(max-min+1));
		if(nD!=nA && nD!=nB && nD!=nC) {
                    break;
		}
            }
            aTemp=new Array(nA,nB,nC,nD);
            aTemp.sort(function(){return Math.floor(Math.random()*3)-1});
            aSoluceNombreCible.push(new Array(new Array(nA,nB),aTemp,aCible[i]));
	}
	
    }
    //on reconstruit le tableau des cibles
    aTemp = new Array();
    for(i=0;i<10;i++) {
	aTemp[i] = aSoluceNombreCible[i][2];
    }
    aCible=aTemp;
    console.log("aCible = "+aCible);
}
//Création de la page titre : 3 éléments exo.blocTitre, exo.blocConsigneGenerale, exo.blocIllustration

exo.creerPageTitre = function() {
    exo.blocTitre.html(exo.txt.titre);
    exo.blocConsigneGenerale.html(exo.txt.consigneGenerale);
    var illustration = disp.createImageSprite(exo,"illustration");
    exo.blocIllustration.html(illustration)
}

//Création de la page question, exécutée à chaque question, tous les éléments de la page doivent être ajoutés à exo.blocAnimation

exo.creerPageQuestion = function() {
    var q = exo.indiceQuestion;

	exo.keyboard.config({
		numeric:"disabled",
		arrow:"disabled",
		large:"inabled",
	});
	
    aNombre = aSoluceNombreCible[q][1];
    aSoluce = aSoluceNombreCible[q][0];
    var texte=exo.txt.consigne1 + aCible[q];
    var affichageQuestion = disp.createTextLabel(texte);
    exo.blocAnimation.append(affichageQuestion);
    affichageQuestion.css({
        left:400,
        top:40
    })
    affichageQuestion.css({
        width:250//Au bour de 250 px, il y a un retour chariot automatique
    })
    for(i=0;i<4;i++) {
        cartouche = disp.createImageSprite(exo,"cartouche");
        exo.blocAnimation.append(cartouche);
        var x=100+(i%2*(120+10));
        var y=40+((Math.floor(i/2))*(120+10));
        var posAt = "left+" + x + " top+" + y;
        cartouche.position({
            my:'left top',
            at:posAt,
            of:exo.blocAnimation
        });
        cartouche.data("valeur",aNombre[i]);
        cartouche.data("glow",0);
        //etiquette
        var etiquette = disp.createTextLabel(aNombre[i]);
        etiquette.css({
            fontSize:32,
            fontWeight:'bold',
            color:'#fff'
        });
        cartouche.append(etiquette);
        etiquette.position({
            my:'center center',
            at:'center center',
            of:cartouche
        });
        aCartouche[i]=cartouche;
        cartouche.on("mousedown.clc touchstart.clc",gestionClickCartouche);
    }
    function gestionClickCartouche(e) {
		e.preventDefault();
        cartoucheClick = $(e.delegateTarget);
        if (cartoucheClick.data("glow")==0) {
            //On a sélectionné le carré
            anim.glow(cartoucheClick,'green',5,5);
            cartoucheClick.data("glow",1);
        } else if (cartoucheClick.data("glow")==1) {
            //On a désélectionné le carré
            anim.glow(cartoucheClick,'green',0,0);
            cartoucheClick.data("glow",0);
        }
    }
}

// Evaluation doit toujours retourner "juste" "faux" ou "rien"

exo.evaluer = function() {
    var q = exo.indiceQuestion;
    reponseEleve=0;
    for(i=0;i<4;i++) {
        if(aCartouche[i].data("glow")==1) {
            reponseEleve += aCartouche[i].data("valeur");
        }
    }
    if (reponseEleve == 0) {
       return "rien" 
    } else if( reponseEleve == aCible[q]) {
        return "juste";
    } else {
        return "faux";
    }
}

// Correction (peut rester vide)

exo.corriger = function() {
    var q = exo.indiceQuestion;
    if (aSoluceNombreCible[q][0].length==2) {
      for(i=0;i<4;i++) {
            anim.glow(aCartouche[i],'green',0,0)
            for(j=0;j<2;j++) {
                if(aCartouche[i].data("valeur") == aSoluceNombreCible[q][0][j]) {
                    anim.glow(aCartouche[i],'red',5,5);          
                }
            }
        }
    } else {
      for(i=0;i<4;i++) {
            anim.glow(aCartouche[i],'green',0,0)
            for(j=0;j<3;j++) {
                if(aCartouche[i].data("valeur") == aSoluceNombreCible[q][0][j]) {
                    anim.glow(aCartouche[i],'red',5,5);          
                }
            }
        }        
    }
}

// Création des contrôles permettant au prof de paraméter l'exo

exo.creerPageParametre = function() {
    var controle = disp.createOptControl(exo,{
        type:"text",
        largeur:48,
        taille:4,
        nom:"tempsQuestion",
        texte:exo.txt.option1
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"totalEssai",
        texte:exo.txt.option2,
        aValeur:[1,2],
        aLabel:exo.txt.valeur2
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"plageCible",
        texte:exo.txt.option3,
        aValeur:["10-20","20-50","50-100"],
        aLabel:exo.txt.valeur3
    });
    exo.blocParametre.append(controle);
    var controle = disp.createOptControl(exo,{
        type:"radio",
        largeur:24,
        taille:2,
        nom:"typeSoluce",
        texte:exo.txt.option4,
        aValeur:[2,0,1],
        aLabel:exo.txt.valeur4
    });
    exo.blocParametre.append(controle);
	
}

/********************
*   C'est fini
*********************/
exo.init();
return exo.baseContainer;
}
return clc;
}(CLC))