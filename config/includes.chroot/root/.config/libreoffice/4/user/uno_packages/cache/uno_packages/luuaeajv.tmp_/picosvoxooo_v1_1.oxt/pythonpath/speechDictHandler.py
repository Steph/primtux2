#!/usr/bin/env python
# -*- coding: utf-8
#speechDictHandler.py
#A part of NonVisual Desktop Access (NVDA)
#Copyright (C) 2006-2007 NVDA Contributors <http://www.nvda-project.org/>
#This file is covered by the GNU General Public License.
#See the file COPYING for more details.

import re
import sys
#from logHandler import log
import os
import codecs
#import synthDriverHandler
#import api
#import config

dictionaries = {}
dictTypes = ("default", "builtin") # ordered by their priority E.G. voice specific speech dictionary is processed before the default

class SpeechDictEntry:

	def __init__(self, pattern, replacement,comment,caseSensitive=True,regexp=False):
		self.pattern = pattern
		flags = re.U
		if not caseSensitive: flags|=re.IGNORECASE
		tempPattern=pattern if regexp else re.escape(pattern)
		self.compiled = re.compile(tempPattern,flags)
		self.replacement = replacement
		self.comment=comment
		self.caseSensitive=caseSensitive
		self.regexp=regexp

	def sub(self, text):
		replacement=self.replacement
		return self.compiled.sub(replacement, text)

class SpeechDict(list):

	def load(self, fileName):
		self.fileName=fileName
		comment=""
		del self[:]
		#log.debug("Loading speech dictionary '%s'..." % fileName)
		if not os.path.isfile(fileName): 
			#log.debug("file '%s' not found." % fileName)
			return
		file = codecs.open(fileName,"r","utf_8_sig",errors="replace")
		for line in file:
			if line.isspace():
				comment=""
				continue
			line=line.rstrip('\r\n')
			if line.startswith('#'):
				if comment:
					comment+=" "
				comment+=line[1:]
			else:
				temp=line.split("\t")
				if len(temp) ==4:
					self.append(SpeechDictEntry(temp[0],temp[1],comment,bool(int(temp[2])),bool(int(temp[3]))))
					comment=""
				else:
					# "can't parse line"
					pass
		#log.debug("%d loaded records." % len(self))
		file.close()
		return

	def sub(self, text):
		for entry in self:
			text = entry.sub(text)
		return text

def processText(text):
	for type in dictTypes:
		text=dictionaries[type].sub(text)
		text = re.compile("[\0\r\n]").sub(" ", text)
	return text

def initialize(langue, bpath = None):
	base_path = bpath
	if base_path is None:
		base_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'speechDicts')
	elif not base_path.endswith('speechDicts'):
		base_path = os.sep.join([base_path, 'speechDicts'])

	for type in dictTypes:
		dictionaries[type]=SpeechDict()
	dictname = "pico_" + langue + ".dic"
	dictionaries["default"].load(os.path.join(base_path,dictname))


