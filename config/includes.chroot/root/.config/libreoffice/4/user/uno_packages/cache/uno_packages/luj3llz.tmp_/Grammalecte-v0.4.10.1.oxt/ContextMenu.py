# -*- coding: utf8 -*-
# Grammalecte - Lexicographe
# by Olivier R. License: MPL 2

import uno
import unohelper
import re
import traceback

from com.sun.star.task import XJob
from com.sun.star.task import XJobExecutor
from com.sun.star.ui import XContextMenuInterceptor
from com.sun.star.ui.ContextMenuInterceptorAction import IGNORED
from com.sun.star.ui.ContextMenuInterceptorAction import EXECUTE_MODIFIED

xHUNSPELL = None
xLOCALE = None
xDESKTOP = None

dTAGS = {   'po:mg': "",
            'po:nom': u" nom,",
            'po:adj': u" adjectif,",
            'po:geo': u" nom,",
            'po:patr': u" patronyme,",
            'po:prn': u" prénom,",
            'po:npr': u" nom propre,",
            'po:adv': u" adverbe,",
            'po:negadv': u" adverbe de négation,",
            'po:prep': u" préposition,",
            'po:prepv': u" préposition verbale,",
            'po:det': u" déterminant,",
            'po:detdem': u" déterminant,",
            'po:detpos': u" déterminant,",
            'po:detind': u" déterminant,",
            'po:detneg': u" déterminant,",
            'po:proind': u" pronom indéfini,",
            'po:proint': u" pronom interrogatif,",
            'po:prorel': u" pronom relatif,",
            'po:propersuj': u" pronom personnel sujet,",
            'po:properobj': u" pronom personnel objet,",
            'po:cjco': u" conjonction de coordination,",
            'po:cjsub': u" conjonction de subordination,",
            'po:loc.adv': u" locution adverbiale (él.),",
            'po:loc.adj': u" locution adjectivale (él.),",
            'po:loc.verb': u" locution verbale (él.),",
            'po:loc.nom': u" locution nominale (él.),",
            'po:loc.prep': u" locution prépositive (él.),",
            'po:interj': u" interjection,",
            'po:nb': u" nombre,",
            'po:v1': u" verbe (1er gr.),",
            'po:v2': u" verbe (2e gr.),",
            'po:v3': u" verbe (3e gr.),",
            'po:v0ei_____a': u" verbe,",
            'po:v0ait____a': u" verbe,",

            'po:1pe': u" 1re pers.,",
            'po:2pe': u" 2e pers.,",
            'po:3pe': u" 3e pers.,",
            
            'is:epi': u" épicène",
            'is:mas': u" masculin",
            'is:fem': u" féminin",
            'is:sg': u" singulier",
            'is:pl': u" pluriel",
            'is:inv': u" invariable",

            'po:infi': u" infinitif,",
            'po:ppre': u" participe présent,",
            'po:ppas': u" participe passé,",

            'po:ipre': u" présent,",
            'po:ifut': u" futur,",
            'po:ipsi': u" passé simple,",
            'po:iimp': u" imparfait,",
            'po:cond': u" conditionnel présent,",
            'po:spre': u" subjonctif présent,",
            'po:simp': u" subjonctif imparfait,",
            'po:impe': u" impératif,",

            'po:1sg': u" 1re pers. sing.,",
            'po:1isg': u" 1re pers. sing. [je?],",
            'po:1jsg': u"",
            'po:2sg': u" 2e pers. sing.,",
            'po:3sg': u" 3e pers. sing.,",
            'po:1pl': u" 1re pers. plur.,",
            'po:2pl': u" 2e pers. plur.,",
            'po:3pl': u" 3e pers. plur.,",
            'po:3pl!': u" 3e pers. plur.,",
        }

dPFX = {
        'd': u"(de), déterminant épicène invariable",
        'l': u"(le/la), déterminant masculin/féminin singulier",
        'j': u"(je), pronom personnel sujet, 1re pers., épicène singulier",
        'm': u"(me), pronom personnel objet, 1re pers., épicène singulier",
        't': u"(te), pronom personnel objet, 2e pers., épicène singulier",
        's': u"(se), pronom personnel objet, 3e pers., épicène singulier/pluriel",
        'n': u"(ne), adverbe de négation",
        'c': u"(ce), pronom démonstratif, masculin singulier/pluriel",
        u'ç': u"(ça), pronom démonstratif, masculin singulier",
        'qu': u"(que), conjonction de subordination",
        'lorsqu': u"(lorsque), conjonction de subordination",
        'quoiqu': u"(quoique), conjonction de subordination",
        'jusqu': u"(jusque), préposition",
       }

dAD = { 'je': u" pronom personnel sujet, 1re pers. sing.",
        'tu': u" pronom personnel sujet, 2e pers. sing.",
        'il': u" pronom personnel sujet, 3e pers. masc. sing.",
        'on': u" pronom personnel sujet, 3e pers. sing. ou plur.",
        'elle': u" pronom personnel sujet, 3e pers. fém. sing.",
        'nous': u" pronom personnel sujet/objet, 1re pers. plur.",
        'vous': u" pronom personnel sujet/objet, 2e pers. plur.",
        'ils': u" pronom personnel sujet, 3e pers. masc. plur.",
        'elles': u" pronom personnel sujet, 3e pers. masc. plur.",
        
        u"là": u" particule démonstrative",
        "ci": u" particule démonstrative",
        
        'le': u" COD, masc. sing.",
        'la': u" COD, fém. sing.",
        'les': u" COD, plur.",
            
        'moi': u" COI (à moi), sing.",
        'toi': u" COI (à toi), sing.",
        'lui': u" COI (à lui ou à elle), sing.",
        'nous2': u" COI (à nous), plur.",
        'vous2': u" COI (à vous), plur.",
        'leur': u" COI (à eux ou à elles), plur.",

        'y': u" pronom adverbial",
        "m'y": u" (me) pronom personnel objet + (y) pronom adverbial",
        "t'y": u" (te) pronom personnel objet + (y) pronom adverbial",
        "s'y": u" (se) pronom personnel objet + (y) pronom adverbial",

        'en': u" pronom adverbial",
        "m'en": u" (me) pronom personnel objet + (en) pronom adverbial",
        "t'en": u" (te) pronom personnel objet + (en) pronom adverbial",
        "s'en": u" (se) pronom personnel objet + (en) pronom adverbial",
      }

reELIDEDPREFIX = re.compile(u"(?i)^([dljmtsncç]|qu|lorsqu|quoiqu|jusqu)['’](.+)")
reCOMPOUNDWORD = re.compile(u"(?i)(\\w+)-((les?|la)-(moi|toi|lui|[nv]ous|leur)|t-(il|elle|on)|y|en|[mts][’'](y|en)|les?|l[aà]|[mt]oi|leur|lui|je|tu|ils?|elles?|on|[nv]ous)$")
reVERBSTEM = re.compile(u"(?i)st:(\\w[\\w-]*).* po:v")

def printServices (o):
    for s in o.getAvailableServiceNames():
        print(' >'+s)

def getConfigSetting (sNodeConfig, bUpdate):
    # get a configuration node
    # example: aSettings = getConfigSetting("/org.openoffice.Office.Common/Path/Current", false)
    xSvMgr = uno.getComponentContext().ServiceManager
    xConfigProvider = xSvMgr.createInstanceWithContext("com.sun.star.configuration.ConfigurationProvider", uno.getComponentContext())
    xPropertyValue = uno.createUnoStruct("com.sun.star.beans.PropertyValue")
    xPropertyValue.Name = "nodepath"
    xPropertyValue.Value = sNodeConfig
    if bUpdate:
        sService = "com.sun.star.configuration.ConfigurationUpdateAccess"
    else:
        sService = "com.sun.star.configuration.ConfigurationAccess"
    return xConfigProvider.createInstanceWithArguments(sService, (xPropertyValue,))


class MyContextMenuInterceptor (XContextMenuInterceptor, unohelper.Base):
    def __init__ (self, ctx):
        self.ctx = ctx

    def notifyContextMenuExecute (self, xEvent):
        sWord = self._getWord()
        if not sWord or not xHUNSPELL.isValid(sWord, xLOCALE, ()) or sWord.count("-") > 4 or sWord.isdigit():
            # Note: isValid also returns True if both parts of compound words exist
            #return uno.Enum("com.sun.star.ui.ContextMenuInterceptorAction", "IGNORED") # don’t work on AOO, have to import the value
            return IGNORED
        try:
            xContextMenu = xEvent.ActionTriggerContainer
            if xContextMenu:
                # entries index
                i = xContextMenu.Count

                # line separator
                xSeparator = xContextMenu.createInstance("com.sun.star.ui.ActionTriggerSeparator") # Unknown object
                xSeparator.SeparatorType = uno.getConstantByName("com.sun.star.ui.ActionTriggerSeparatorType.LINE")
                xContextMenu.insertByIndex(i, xSeparator)
                i = i + 1
                
                # special cases
                m1 = reELIDEDPREFIX.match(sWord)
                if m1:
                    sWord = m1.group(2)
                m2 = reCOMPOUNDWORD.match(sWord)
                if m2:
                    sWord = m2.group(1)
                    
                # Morphologies
                if m1:
                    # elided prefix
                    xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xMenuItem.setPropertyValue( "Text", u"%s’ : %s" % (m1.group(1), dPFX.get(m1.group(1).lower(), "[?]")) )
                    xContextMenu.insertByIndex(i, xMenuItem)
                    i = i + 1
                
                lMorph = self._analyzeWord(sWord)
                if len(lMorph) > 1:
                    # submenu
                    # create root menu entry
                    xRootMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xRootMenuItem.setPropertyValue("Text", sWord)
                    xSubMenuContainer = xContextMenu.createInstance("com.sun.star.ui.ActionTriggerContainer")
                    xRootMenuItem.setPropertyValue("SubContainer", xSubMenuContainer)
                    # add entries
                    for s in lMorph:
                        xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                        xMenuItem.setPropertyValue("Text", self._formatResult(s))
                        xSubMenuContainer.insertByIndex(0, xMenuItem)
                    # add new sub menu into the context menu
                    xContextMenu.insertByIndex(i, xRootMenuItem)
                elif len(lMorph) == 1:
                    # one entry
                    xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xMenuItem.setPropertyValue( "Text", u"%s : %s" % (sWord, self._formatResult(lMorph[0])) )
                    #xMenuItem.setPropertyValue( "CommandURL", u"grammalecte.Tools" )
                    xContextMenu.insertByIndex(i, xMenuItem)
                else:
                    xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xMenuItem.setPropertyValue("Text", u"%s :  inconnu du dictionnaire" % sWord)
                    xContextMenu.insertByIndex(i, xMenuItem)
                
                if m2:
                    # last part
                    i = i + 1
                    xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xMenuItem.setPropertyValue( "Text", u"-%s : %s" % (m2.group(2), self._getAddon(m2.group(2).lower())) )
                    xContextMenu.insertByIndex(i, xMenuItem)
                
                # Conjugueur
                aVerb = set()
                for s in lMorph:
                    m = reVERBSTEM.search(s)
                    if m and m.group(1) not in aVerb:
                        aVerb.add(m.group(1))
                for sVerb in aVerb:
                    i = i + 1
                    xMenuItem = xContextMenu.createInstance("com.sun.star.ui.ActionTrigger")
                    xMenuItem.setPropertyValue("Text", u"Conjuguer “%s”…" % sVerb)
                    xMenuItem.setPropertyValue("CommandURL", "service:net.grammalecte.AppLauncher?CJ/"+sVerb)
                    xContextMenu.insertByIndex(i, xMenuItem)

                # The controller should execute the modified context menu and stop notifying other interceptors.
                #return uno.Enum("com.sun.star.ui.ContextMenuInterceptorAction", "EXECUTE_MODIFIED") # don’t work on AOO, have to import the value
                return EXECUTE_MODIFIED
        except:
            traceback.print_exc()
        #return uno.Enum("com.sun.star.ui.ContextMenuInterceptorAction", "IGNORED") # don’t work on AOO, have to import the value
        return IGNORED

    def _getWord (self):
        try:
            xDoc = xDESKTOP.getCurrentComponent()
            xViewCursor = xDoc.CurrentController.ViewCursor
            if xViewCursor.CharLocale.Language != "fr":
                return ""
            xText = xViewCursor.Text
            xCursor = xText.createTextCursorByRange(xViewCursor)
            xCursor.gotoStartOfWord(False)
            xCursor.gotoEndOfWord(True)
        except:
            traceback.print_exc()
        return xCursor.String.strip('.')

    def _analyzeWord (self, word):
        if not word:
            return []
        if not xHUNSPELL.hasLocale(xLOCALE):
            return []
        x = xHUNSPELL.spell("<?xml?><query type='analyze'><word>%s</word></query>" % word, xLOCALE, ())
        if not x:
            return []
        t = x.getAlternatives()
        if not t:
            return []
        sRes = t[0].replace('</a>', "\n").replace('<a>', '').replace('<code>', '').replace('</code>', '').strip("\n")
        return sRes.split("\n")
    
    def _formatResult (self, sTags):
        sRes = ""
        sTags = re.sub(" +ph:.*$", "", sTags)
        sTags = re.sub("(?<=v[1-3])[itpqnmr_!ea?]+", "", sTags)
        lTag = sTags.split()
        sStem = lTag.pop(0)
        for s in lTag:
            sRes += dTAGS.get(s, " [%s]" % s)
        if sRes.startswith(" verbe") and not sRes.endswith("infinitif"):
            sRes += " [%s]" % sStem[3:]
        return sRes.rstrip(",")

    def _getAddon (self, s):
        sRes = u""
        if s.startswith("t-"):
            return u"“t” euphonique +" + dAD.get(s[2:], "[?]")
        if not "-" in s:
            return dAD.get(s.replace(u"’", "'"), "[?]")
        if s.endswith("ous"):
            s += '2'
        pos = s.find("-")
        return u"%s +%s" % (dAD.get(s[:pos], "[?]"), dAD.get(s[pos+1:], "[?]"))


class JobExecutor (XJob, unohelper.Base):
    def __init__ (self, ctx):
        self.ctx = ctx
        global xDESKTOP
        global xHUNSPELL
        global xLOCALE
        xDESKTOP = self.ctx.getServiceManager().createInstanceWithContext('com.sun.star.frame.Desktop', self.ctx)
        xCurCtx = uno.getComponentContext()
        xHUNSPELL = xCurCtx.ServiceManager.createInstanceWithContext("com.sun.star.linguistic2.SpellChecker", xCurCtx)
        xLOCALE = uno.createUnoStruct('com.sun.star.lang.Locale')
        xLOCALE.Language = 'fr'
        xLOCALE.Country = 'FR'
        
    def execute (self, args):
        if not args:
            return
        # what version of the software?
        xSettings = getConfigSetting("org.openoffice.Setup/Product", False)
        sProdName = xSettings.getByName("ooName")
        sVersion = xSettings.getByName("ooSetupVersion")
        if (sProdName == "LibreOffice" and sVersion < "4") or sProdName == "OpenOffice.org":
            return
        
        # what event?
        bCorrectEvent = False
        for arg in args:
            if arg.Name == "Environment":
                for v in arg.Value:
                    if v.Name == "EnvType" and v.Value == "DOCUMENTEVENT":
                        bCorrectEvent = True
                    elif v.Name == "EventName":
                        pass
                        # check is correct event
                        #print "Event: %s" % v.Value
                    elif v.Name == "Model":
                        model = v.Value
        if bCorrectEvent:
            if model.supportsService("com.sun.star.text.TextDocument"):
                xController = model.getCurrentController()
                if xController:
                    xController.registerContextMenuInterceptor(MyContextMenuInterceptor(self.ctx))
        

g_ImplementationHelper = unohelper.ImplementationHelper()
g_ImplementationHelper.addImplementation(JobExecutor, "grammalecte.ContextMenuHandler", ("grammalecte.ContextMenuHandler",),)
